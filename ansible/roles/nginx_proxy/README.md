Nginx Proxy
=========

Role intended for Dyna nginx proxy pass confgiuration.

nginx proxy should be created in public network, to handle traffic from route53 and for letsencrypt.

nginx proxy handle all public traffic and off load the SSL traffic to private servers.

Requirements
------------

To run certbot certificates, https should be open to Letencrypt servers, else letsencrypt cant write back to the servers.

Role Variables
--------------
```
nginx_proxy:
  nginx_cert: dynacommercelab.com
```
Its important to set this variable once you create the certificate. So the site-enable will use this variable to set the certificate correct.


Example: To run the playbook
----------------
```
    - hosts: nginx-proxy
      roles:
         - { role: nginx_proxy }
```

```
ansible-playbook -i inventories/development/nginx-proxy/hosts playbooks/install/nginx-proxy.yml

ansible-playbook -i inventories/development/nginx-proxy/hosts playbooks/install/nginx-proxy.yml --tags certificate

ansible-playbook -i inventories/development/nginx-proxy/hosts playbooks/install/nginx-proxy.yml --tags site_enable
```

License
-------

Dyna

Author Information
------------------

ranjith.ka@dynacommerce.com
