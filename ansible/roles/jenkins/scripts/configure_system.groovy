/*
   This script will configure global settings for a Jenkins instance.  These
   are all settings built into core.
   Configures Global settings:
     - Jenkins URL
     - Admin email
     - System message
     - Quiet Period
     - SCM checkout retry count
     - Number of master executors
     - Master labels
     - Master usage (e.g. exclusive or any job runs on master)
     - TCP port for JNLP slave agents
 */

import jenkins.model.Jenkins
import jenkins.model.JenkinsLocationConfiguration
import groovy.json.JsonSlurper

def system_settings_json = '''
${system_settings}
'''

def master_settings = new JsonSlurper().parseText(system_settings_json)

def requiredDefaultValues(def value, List values, def default_value) {
  (value in values)? value : default_value
}

if (!(master_settings instanceof Map)) {
  throw new Exception('master_settings must be a Map.')
}

//settings with sane defaults
String frontend_url = master_settings.frontend_url ?: 'http://localhost:8080/'
String admin_email = master_settings.admin_email ?: 'Jenkins <jenkins@dynacommerce.com>'
String system_message = master_settings.system_message ?: ''
int quiet_period = master_settings.quiet_period ?: 5
int scm_checkout_retry_count = master_settings.scm_checkout_retry_count ?: 0
int master_executors = master_settings.master_executors ?: 1
String master_labels = master_settings.master_labels ?: 'master'
String master_usage = requiredDefaultValues(master_settings.master_usage?.toUpperCase(), ['NORMAL', 'EXCLUSIVE'], 'EXCLUSIVE')
int jnlp_slave_port = master_settings.jnlp_slave_port ?: -1

Jenkins instance = Jenkins.getInstance()
JenkinsLocationConfiguration location = JenkinsLocationConfiguration.get()
Boolean save = false

if (location.getUrl() != frontend_url) {
  location.setUrl(frontend_url)
  save = true
}
if (admin_email && location.adminAddress != admin_email) {
  location.adminAddress = admin_email
  save = true
}
if (instance.systemMessage != system_message) {
  instance.systemMessage = system_message
  save = true
}
if (instance.quietPeriod != quiet_period) {
  instance.setQuietPeriod(quiet_period)
  save = true
}
if (instance.scmCheckoutRetryCount != scm_checkout_retry_count) {
  instance.scmCheckoutRetryCount = scm_checkout_retry_count
  save = true
}
if (instance.numExecutors != master_executors) {
  instance.setNumExecutors(master_executors)
  save = true
}
if (instance.labelString != master_labels) {
  instance.setLabelString(master_labels)
  save = true
}
if (instance.mode.toString() != master_usage) {
  instance.mode = Node.Mode[master_usage]
  save = true
}
if (instance.slaveAgentPort != jnlp_slave_port) {
  if (jnlp_slave_port <= 65535 && jnlp_slave_port >= -1) {
    instance.slaveAgentPort = jnlp_slave_port
    save = true
  }
}

//save configuration to disk
if (save) {
  instance.save()
  location.save()
}

return save ? "config updated" : "nothing changed"
