import jenkins.model.Jenkins
import groovy.json.JsonSlurper

def slack_settings_json = '''
${slack_settings_args}
'''
def slack_settings = new JsonSlurper().parseText(slack_settings_json)
def instance = Jenkins.getInstance()
def slack = instance.getDescriptorByType(jenkins.plugins.slack.SlackNotifier.DescriptorImpl.class)

slack.setTeamDomain(slack_settings.teamDomain)
slack.setTokenCredentialId(slack_settings.tokenCredentialId)
slack.setRoom(slack_settings.room)


save = true
slack.save()
instance.save()

return save ? "config updated" : "nothing changed"
