hosts:
  mysql: "192.168.100.101"
  redis: "192.168.100.101"
  rabbitmq: "192.168.100.101"
  ose: "192.168.100.100"
  oil: "192.168.100.103"

rabbitmq:
  admin_username: admin

mysql:
  admin_user: dynaadmin
  dyna_admin_password: "{{ vault.mysql.dyna_admin_password }}"

nginx:
  rpm: "nginx-1.12.2-1.el7_4.ngx.x86_64.rpm"
  media_browse: true
  vhosts:
    - server_name: uct
      port: 9021
      web_root: /opt/omnius/uct/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-uct
      includes:
        - name: omnius-uct
          template: php
          fpm_pool: omnius-uct-php-fpm
    - server_name: um
      port: 9022
      web_root: /opt/omnius/um/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-um
      includes:
        - name: omnius-um
          template: php
          fpm_pool: omnius-um-php-fpm
    - server_name: sales-api
      port: 9023
      web_root: /opt/omnius/restapi/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-sales-api
      includes:
        - name: omnius-sales-api
          template: php
          fpm_pool: omnius-sales-api-php-fpm
    - server_name: telesales-dev-infra.dynacommercelab.com
      port: 80
      web_root: /opt/omnius/ose
      log_dir: /var/log/nginx/omnius
      log_name: ose-telesales
      includes:
        - name: ose-telesales
          template: mage-php
          fpm_pool: omnius-ose-php-fpm
          mage_run_code: telesales
          mage_env_code: BAU
          mage_run_type: store
          mage_dev_mode: true
          https: "off"
        - name: uct-proxy
          template: api-proxy
          rewrite: ^/webhooks/v1/uct/(.*)$ /$1 break;
          path: /webhooks/v1/uct
          pass: "http://{{ hosts.ose }}:9021"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'telesales-dev-infra.dynacommercelab.com';"
        - name: um-proxy
          template: api-proxy
          rewrite: ^/usermanagement/api/v1/(.*)$ /$1 break;
          path: /usermanagement/api/v1
          pass: "http://{{ hosts.ose }}:9022"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'telesales-dev-infra.dynacommercelab.com';"
        - name: sales-api
          template: api-proxy
          rewrite: ^/restapi/api/v1/(.*)$ /$1 break;
          path: /restapi/api/v1
          pass: "http://{{ hosts.ose }}:9023"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'telesales-dev-infra.dynacommercelab.com';"
php:
  version: 71
  repo_path: "http://rpms.remirepo.net/enterprise/remi-release-7.rpm"
  settings:
    - section: Date
      option: date.timezone
      value: Europe/Amsterdam
    - section: PHP
      option: max_execution_time
      value: 18000
    - section: PHP
      option: memory_limit
      value: 512M
    - section: PHP
      option: max_input_vars
      value: 2000
    - section: PHP
      option: allow_url_include
      value: "On"
    - section: PHP
      option: error_reporting
      value: E_ALL

sftp:
  groupname: sftpusers
  dirs:
    - /var/sftp/catalog
    - /var/sftp/stubs
  users:
    - username: sftp-ose
      homedir: /var/sftp
      group: sftpusers
  append:
    - omnius-ose
  symlink: yes

ose:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-ose-4.4.0-SNAPSHOT.noarch.rpm"
  dbseed: "https://nexus.dynacommercelab.com/repository/db-seeds/vfde/omnius-ose-4.4.0.sql.gz"
  mysql:
    host: "{{ hosts.mysql }}"
    port: 3306
    db: "ose_st1"
    username: "ose_st1"
    password: "{{ vault.ose.mysql.password }}"
    encoding: "utf8"
  redis_session:
    host: "{{ hosts.redis }}"
    password: "{{ vault.ose.redis_session.password }}"
    port: 6379
    db: 0
    log_level: 10
  backend_options:
    server: "{{ hosts.redis }}"
    password: "{{ vault.ose.backend_options.password }}"
    port: 6379
    database: 1
  logging:
    redis:
      active: 0
      host: "{{ hosts.redis }}"
      password: "{{ vault.ose.logging.redis.password }}"
      database: 10
      port: 6379
      keyPrefix: omnius-ose
      stream: default
      loglevel: DEBUG
    stream:
      active: 1
      loglevel: DEBUG
  vfde:
    frontname: implement
    redis_loglevel: DEBUG
    filePermission: "0744"
    
  storeviews:
    stores:
      - name: telesales
        base_url: http://telesales-dev-infra.dynacommercelab.com/
        scope_id: 0
  configuration:
    - key: web/unsecure/base_url
      value: http://telesales-dev-infra.dynacommercelab.com/

  phpfpm:
    pools:
      - name: omnius-ose
        settings:
          - option: user
            value: omnius-ose
          - option: group
            value: omnius-ose
          - option: listen
            value: /run/php-fpm/omnius-ose-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  rabbitmq:
    vhost: st1
    exchange:
      - dyna.oil.dlx
      - dyna.ose.direct
      - dyna.ose.dlx
      - amdocs.ogw.dlx
      - amdocs.ogw.direct
      - dyna.oil.direct
      - dyna.ose.jobs.direct
      - dyna.ose.setstatus
      - jobs_exchange
      - dyna.ose.headers
    queue_args:
      - name: amdocs.ogw.pushcustomerdata
        exchange: "amdocs.ogw.dlx"
      - name: amdocs.ogw.customerupdate.dlq
      - name: dyna.oil.saporders.archive
        ttl: 86400000
      - name: amdocs.ogw.quotes
      - name: amdocs.ogw.pushorderstatus.dlq
        exchange: "amdocs.ogw.dlx"
      - name: amdocs.ogw.quotes.dlq
      - name: dyna.oil.workitems
        exchange: "dyna.oil.dlx"
      - name: TestQueue
      - name: dyna.oil.saporders
        exchange: "dyna.oil.dlx"
      - name: dyna.oil.workitems.dlq
      - name: amdocs.ogw.customerupdate
        exchange: "amdocs.ogw.dlx"
      - name: dyna.ose.customerupdate.dlq
      - name: TestPushOrderSTatus
      - name: dyna.ose.jobs
      - name: dyna.ose.saporders
        exchange: "dyna.ose.dlx"
      - name: dyna.ose.retry
        exchange: "dyna.ose.jobs.direct"
        routing: "dyna.ose.jobs"
        ttl: 20000
      - name: dyna.oil.saporders.dlq
      - name: amdocs.ogw.pushcustomerdata.dlq
      - name: dyna.ose.delayqueue
        exchange: jobs_exchange
        routing: dyna.ose.jobs
        ttl: 10000
      - name: dyna.ose.setstatus
      - name: dyna.ose.customerupdate
        exchange: "dyna.ose.dlx"
      - name: dyna.ose.pushorderstatus.dlq
      - name: dyna.ose.saporders.dlq
      - name: dyna.ose.pushorderstatus
        exchange: "dyna.ose.dlx"
      - name: amdocs.ogw.pushorderstatus
        exchange: "amdocs.ogw.dlx"

    bindings:
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.customerupdate
        destination_type: queue
        routing_key: amdocs.ogw.customerupdate
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.pushcustomerdata
        destination_type: queue
        routing_key: amdocs.ogw.pushcustomerdata
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.pushorderstatus
        destination_type: queue
        routing_key: amdocs.ogw.pushorderstatus
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.quotes
        destination_type: queue
        routing_key: amdocs.ogw.quotes
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.customerupdate.dlq
        destination_type: queue
        routing_key: amdocs.ogw.customerupdate
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.customerupdate.dlq
        destination_type: queue
        routing_key: amdocs.ogw.pushcustomerdata
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.customerupdate.dlq
        destination_type: queue
        routing_key: amdocs.ogw.pushorderstatus
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.quotes.dlq
        destination_type: queue
        routing_key: amdocs.ogw.quotes
      - name: dyna.oil.direct
        destination: dyna.oil.workitems
        destination_type: queue
        routing_key: dyna.oil.workitems
      - name: dyna.oil.direct
        destination: dyna.ose.saporders
        destination_type: queue
        routing_key: dyna.ose.saporders
      - name: dyna.oil.direct
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: dyna.ose.setstatus
      - name: dyna.oil.dlx
        destination: dyna.oil.saporders.dlq
        destination_type: queue
        routing_key: dyna.oil.saporders
      - name: dyna.oil.dlx
        destination: dyna.oil.workitems.dlq
        destination_type: queue
        routing_key: dyna.oil.workitems
      - name: dyna.ose.direct
        destination: amdocs.ogw.quotes
        destination_type: queue
        routing_key: amdocs.ogw.quotes
      - name: dyna.ose.direct
        destination: dyna.oil.saporders
        destination_type: queue
        routing_key: dyna.oil.saporders
      - name: dyna.ose.direct
        destination: dyna.oil.saporders.archive
        destination_type: queue
        routing_key: dyna.oil.saporders
      - name: dyna.ose.direct
        destination: dyna.oil.workitems
        destination_type: queue
        routing_key: dyna.oil.workitems
      - name: dyna.ose.direct
        destination: dyna.ose.saporders
        destination_type: queue
        routing_key: dyna.ose.saporders
      - name: dyna.ose.direct
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: dyna.ose.setstatus
      - name: dyna.ose.dlx
        destination: dyna.ose.customerupdate.dlq
        destination_type: queue
        routing_key: dyna.ose.customerupdate
      - name: dyna.ose.dlx
        destination: dyna.ose.pushorderstatus.dlq
        destination_type: queue
        routing_key: dyna.ose.pushorderstatus
      - name: dyna.ose.dlx
        destination: dyna.ose.saporders.dlq
        destination_type: queue
        routing_key: dyna.ose.saporders
      - name: dyna.ose.headers
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: dyna.ose.setstatus
      - name: dyna.ose.jobs.direct
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: ""
      - name: dyna.ose.jobs.direct
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: dyna.ose.jobs
      - name: dyna.ose.jobs.direct
        destination: dyna.ose.retry
        destination_type: queue
        routing_key: dyna.ose.retry
      - name: dyna.ose.setstatus
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: ""
      - name: jobs_exchange
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: ""
      - name: jobs_exchange
        destination: dyna.ose.delayqueue
        destination_type: queue
        routing_key: dyna.ose.delayqueue
      - name: jobs_exchange
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: dyna.ose.jobs

vfde_dbextract:
  mysql:
    db: "dbextract"
    encoding: "utf8"
    dbseed: "https://nexus.dynacommercelab.com/repository/db-seeds/vfde/omnius-ose-dbextract-4.7.0-SNAPSHOT.sql.gz"

influxdb:
  admin_user: admin
  host: https://telegraf-influxdb.dynacommercelab.com
  admin_password: "{{ vault.influxdb.admin_password }}"
  users:
    - name: vfdesapdaemon
      database: vfdesapdaemon
      password: "{{ vault.sapdaemon.influxdb_password }}"

    - name: vfdestatusdaemon
      database: vfdestatusdaemon
      password: "{{ vault.statusdaemon.influxdb_password }}"

    - name: vfdejobdaemon
      database: vfdejobdaemon
      password: "{{ vault.jobdaemon.influxdb_password }}"

jobdaemon:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/jobdaemon/omnius-jobdaemon-1.1-SNAPSHOT.noarch.rpm
  rabbitmq_password: "{{ vault.rabbitmq.oil }}"
  influxdb_password: "{{ vault.jobdaemon.influxdb_password }}"
  config:
    php_exec_path: /usr/bin/php
    php_script: /opt/omnius/ose/shell/job_processor_runner.php
    max_threads: 50
    rabbitmq:
      username: dyna.oil
      hostname: "{{ hosts.rabbitmq }}"
      vhost: st1
      port: 5672
      queue_job: dyna.ose.jobs
      queue_retry_exchange: dyna.ose.jobs.direct
      queue_retry: dyna.ose.retry
      ssl:
        enabled: "false"
        accept_policy_errors: "false"
        skip_validation: "false"
      max_retries: 10
      receive_timeout: 1000
      message_interval: 10
      wait_interval: 0
      max_messages: 100
    influxdb:
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: "vfdejobdaemon"
      database: "vfdejobdaemon"
      inflow: jobdaemon_inflow
      delay: jobdaemon_delay
      ack: jobdaemon_ack
    log_path: /var/log/jobdaemon/
    redis:
      host: "{{ hosts.redis }}"
      port: 6379
      expire: 3600
      db: 2

statusdaemon:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/statusdaemon/omnius-statusdaemon-1.1.0-1.noarch.rpm
  config:
    php_script: /opt/omnius/ose/shell/statusdaemon/esb_parser.php
    rabbitmq:
      username: dyna.oil
      password: "{{ vault.rabbitmq.oil }}"
      hostname: "{{ hosts.rabbitmq }}"
      vhost: st1
      port: 5672
      queue: dyna.ose.setstatus
      receive_timeout: 1000
      message_interval: 10
      wait_internal: 0
      max_messages: 100
    influxdb:
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: vfdestatusdaemon
      password: "{{ vault.statusdaemon.influxdb_password }}"
      database: vfdestatusdaemon
      inflow: statusdaemon_inflow
      delay: statusdaemon_delay
    log_path: /var/log/omnius/statusdaemon/
    redis:
      host: "{{ hosts.redis }}"
      port: 6379
      expire: 3600
      db: 3

sapdaemon:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-sapdaemon-1.4-SNAPSHOT.noarch.rpm"
  config:
    mysql:
      host: "{{ hosts.mysql }}"
      username: "{{ ose.mysql.username }}"
      password: "{{ vault.ose.mysql.password }}"
      db: "{{ ose.mysql.db }}"
    ose:
      host: "{{ hosts.rabbitmq }}"
      port: 5672
      vhost: st1
      name: dyna.ose.saporders
      username: dyna.ose
      password: "{{ vault.sapdaemon.rabbitmq_password }}"
      ssl: "false"
      ssl_accept_policy_errors: "false"
      ssl_skip_validation: "true"
    oil:
      host: "{{ hosts.rabbitmq }}"
      port: 5672
      vhost: st1
      name: dyna.oil.saporders
      username: ose
      password: "{{ vault.sapdaemon.rabbitmq_password }}"
      ssl: "false"
      ssl_accept_policy_errors: "false"
      ssl_skip_validation: "true"
    move_immediately: "false"
    move_polling_times: 5
    max_messages: 250
    logging:
      extra: "true"
      path: "/var/log/omnius/sapdaemon/"
      maxsize: 10240
    delete_invalid_after_days: 30
    processorder_only: "false"
    add_xml_declaration: "true"
    statusses_to_send: OnHold-PendingHWFulfillment
    influxdb:
      enabled: "true"
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: "vfdesapdaemon"
      password: "{{ vault.sapdaemon.influxdb_password }}"
      database: "vfdesapdaemon"
      inflow: sapdaemon_inflow
      delay: sapdaemon_delay
      ack: "sapdaemon_ack"

vfde_uct:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-uct-api-4.0-SNAPSHOT.noarch.rpm
  phpfpm:
    pools:
      - name: omnius-uct
        settings:
          - option: user
            value: omnius-uct
          - option: group
            value: omnius-uct
          - option: listen
            value: /run/php-fpm/omnius-uct-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: st1
    - section: null
      option: APP_DEBUG
      value: true
    - section: null
      option: APP_KEY
      value: base64:SmVXKPvMl3lxk3KssOU3V8LvZ8v5JTMITSarlY7jx4c=
    - section: null
      option: DB_CONNECTION
      value: mysql
    - section: null
      option: DB_HOST
      value: "{{ hosts.mysql }}"
    - section: null
      option: DB_PORT
      value: "{{ ose.mysql.port }}"
    - section: null
      option: DB_DATABASE
      value: "{{ ose.mysql.db }}"
    - section: null
      option: DB_USERNAME
      value: "{{ ose.mysql.username }}"
    - section: null
      option: DB_PASSWORD
      value: "{{ vault.ose.mysql.password }}"
    - section: null
      option: REDIS_CLUSTER
      value: false
    - section: null
      option: REDIS_HOST
      value: "{{ hosts.redis }}"
    - section: null
      option: REDIS_PORT
      value: "{{ ose.redis_session.port }}"
    - section: null
      option: REDIS_DATABASE
      value: "4"
    - section: null
      option: REDIS_PASSWORD
      value: "{{ vault.ose.redis_session.password }}"
    - section: null
      option: CACHE_DRIVER
      value: memcached
    - section: null
      option: QUEUE_DRIVER
      value: sync
    - section: null
      option: DIGEST_REALM
      value: uct@telesales-dev-infra.dynacommercelab.com
    - section: null
      option: DIGEST_USER
      value: dyna-uct
    - section: null
      option: DIGEST_PASS
      value: Dyna123
    - section: null
      option: DOMAIN_URL_INTERNAL
      value: http://telesales-dev-infra.dynacommercelab.com/uct/
    - section: null
      option: API_URL_INTERNAL
      value: telesales-dev-infra.dynacommercelab.com
    - section: null
      option: DOMAIN_URL_EXTERNAL
      value: http://telesales-dev-infra.dynacommercelab.com/uct/
    - section: null
      option: API_URL_EXTERNAL
      value: telesales-dev-infra.dynacommercelab.com
    - section: null
      option: DOMAIN_URL
      value: http://telesales-dev-infra.dynacommercelab.com/uct/

vfde_um:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-um-api-4.0-SNAPSHOT.noarch.rpm
  phpfpm:
    pools:
      - name: omnius-um
        settings:
          - option: user
            value: omnius-um
          - option: group
            value: omnius-um
          - option: listen
            value: /run/php-fpm/omnius-um-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: st1
    - section: null
      option: APP_DEBUG
      value: true
    - section: null
      option: APP_KEY
      value: base64:SmVXKPvMl3lxk3KssOU3V8LvZ8v5JTMITSarlY7jx4c=
    - section: null
      option: DEFAULT_PASS
      value: secret
    - section: null
      option: DB_CONNECTION
      value: mysql
    - section: null
      option: DB_HOST
      value: "{{ hosts.mysql }}"
    - section: null
      option: DB_PORT
      value: 3306
    - section: null
      option: DB_DATABASE
      value: "{{ ose.mysql.db }}"
    - section: null
      option: DB_USERNAME
      value: "{{ ose.mysql.username }}"
    - section: null
      option: DB_PASSWORD
      value: "{{ vault.ose.mysql.password }}"
    - section: null
      option: CACHE_DRIVER
      value: memcached
    - section: null
      option: QUEUE_DRIVER
      value: sync
    - section: null
      option: DIGEST_REALM
      value: usermanagement@telesales-dev-infra.dynacommercelab.com
    - section: null
      option: DIGEST_USER
      value: dyna-um
    - section: null
      option: DIGEST_PASS
      value: Dyna123
    - section: null
      option: MAGENTO_PASSWORD_SALT
      value: salt
    - section: null
      option: DEFAULT_STORE_ID
      value: 2
    - section: null
      option: DEFAULT_ROLE_ID
      value: 1
    - section: null
      option: DEFAULT_DEALER_ID
      value: 1
    - section: null
      option: DEFAULT_IS_ACTIVE
      value: 1
    - section: null
      option: DEFAULT_TEMPORARY_PASSWORD
      value: 1

sales_api:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-rest-api-1.0.5-SNAPSHOT.noarch.rpm"
  phpfpm:
    pools:
      - name: omnius-sales-api
        settings:
          - option: user
            value: omnius-restapi
          - option: group
            value: omnius-restapi
          - option: listen
            value: /run/php-fpm/omnius-sales-api-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: dev
    - section: null
      option: APP_DEBUG
      value: true
    - section: null
      option: APP_KEY
      value: base64:SmVXKPvMl3lxk3KssOU3V8LvZ8v5JTMITSarlY7jx4c=
    - section: null
      option: DB_CONNECTION
      value: sqlite
    - section: null
      option: CACHE_DRIVER
      value: redis
    - section: null
      option: SESSION_DRIVER
      value: cookie
    - section: null
      option: MAIL_DRIVER
      value: smtp
    - section: null
      option: STUBS_PATH
      value: /opt/omnius/ose/var/stubs/
    - section: null
      option: MAGENTO_STORE
      value: 2
    - section: null
      option: MAGENTO_BASE_PATH
      value: /opt/omnius/ose/
    - section: null
      option: REDIS_HOST
      value: "{{ hosts.redis }}"
    - section: null
      option: REDIS_PORT
      value: 6379
    - section: null
      option: REDIS_PASSWORD
      value: "{{ vault.ose.redis_session.password }}"
    - section: null
      option: REDIS_DATABASE
      value: 3
    - section: null
      option: TAG_BAN
      value: "ban:"
    - section: null
      option: TAG_CUSTOMER_NO
      value: "customer:"
    - section: null
      option: TAG_OFFERS
      value: "offers:"
    - section: null
      option: TAG_JOBS
      value: "jobs:"
    - section: null
      option: STATUS_OFFER_SUBMITTED
      value: "Submitted"
    - section: null
      option: STATUS_OFFER_EXPIRED
      value: "OutOfDate"
    - section: null
      option: STATUS_OFFER_VALID
      value: "Valid"
    - section: null
      option: AMQP_HOST
      value: "{{ hosts.rabbitmq }}"
    - section: null
      option: AMQP_PORT
      value: 5672
    - section: null
      option: AMQP_USER
      value: dyna.oil
    - section: null
      option: AMQP_PASS
      value: "{{ vault.rabbitmq.oil }}"
    - section: null
      option: AMQP_VHOST
      value: st1
    - section: null
      option: AMQP_EXCHANGE
      value: dyna.ose.jobs.direct
    - section: null
      option: AMQP_QUEUE_NAME
      value: dyna.ose.jobs
    - section: null
      option: AMQP_CONSUMER_TAG
      value: consumer
    - section: null
      option: BASIC_USERNAME
      value: dyna-restapi
    - section: null
      option: BASIC_PASSWORD
      value: Dyna123
    - section: null
      option: LOG_PATH
      value: /var/log/omnius/restapi/lumen.log
