hosts:
  mysql: ip-20-0-1-43.eu-central-1.compute.internal
  redis: ip-20-0-1-109.eu-central-1.compute.internal
  rabbitmq: ip-20-0-1-104.eu-central-1.compute.internal
  ose: ip-20-0-1-193.eu-central-1.compute.internal

redis:
  ose:
    redis_session: 42
    backend_options: 43
    logging: 44
  jobdaemon: 45
  statusdaemon: 46
  vfde_uct: 47
  vfde_um: 48
  sales_api: 49

rabbitmq:
  admin_username: admin

mysql:
  admin_user: dynaadmin
  dyna_admin_password: "{{ vault.mysql.dyna_admin_password }}"

sftp:
  groupname: sftpusers
  dirs:
    - /var/sftp/catalog
    - /var/sftp/stubs
  users:
    - username: sftp-ose
      homedir: /var/sftp
      group: sftpusers
  append:
    - omnius-ose
  symlink: yes

nginx:
  rpm: "nginx-1.12.2-1.el7_4.ngx.x86_64.rpm"
  media_browse: true
  vhosts:
    - server_name: uct
      port: 9021
      web_root: /opt/omnius/uct/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-uct
      includes:
        - name: omnius-uct
          template: php
          fpm_pool: omnius-uct-php-fpm
    - server_name: um
      port: 9022
      web_root: /opt/omnius/um/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-um
      includes:
        - name: omnius-um
          template: php
          fpm_pool: omnius-um-php-fpm
    - server_name: sales-api
      port: 9023
      web_root: /opt/omnius/restapi/public
      log_dir: /var/log/nginx/omnius
      log_name: omnius-sales-api
      includes:
        - name: omnius-sales-api
          template: php
          fpm_pool: omnius-sales-api-php-fpm
    - server_name: st6-telesales-vfde.dynacommercelab.com
      port: 80
      web_root: /opt/omnius/ose
      log_dir: /var/log/nginx/omnius
      log_name: ose-telesales
      includes:
        - name: ose-telesales
          template: mage-php
          fpm_pool: omnius-ose-php-fpm
          mage_run_code: telesales
          mage_env_code: BAU
          mage_run_type: store
          mage_dev_mode: false
          https: "on"
        - name: uct-proxy
          template: api-proxy
          path: /webhooks/v1/uct
          pass: "http://{{ hosts.ose }}:9021"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'st6-telesales-vfde.dynacommercelab.com';"
        - name: um-proxy
          template: api-proxy
          path: /usermanagement/api/v1
          pass: "http://{{ hosts.ose }}:9022"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'st6-telesales-vfde.dynacommercelab.com';"
        - name: sales-api
          template: api-proxy
          rewrite: ^/restapi/api/v1/(.*)$ /$1 break;
          path: /restapi/api/v1
          pass: "http://{{ hosts.ose }}:9023"
          extras:
            - "add_header 'Access-Control-Allow-Origin' 'st6-telesales-vfde.dynacommercelab.com';"

php:
  version: 71
  repo_path: "http://rpms.remirepo.net/enterprise/remi-release-7.rpm"
  settings:
    - section: Date
      option: date.timezone
      value: Europe/Amsterdam
    - section: PHP
      option: max_execution_time
      value: 18000
    - section: PHP
      option: memory_limit
      value: 512M
    - section: PHP
      option: max_input_vars
      value: 2000
    - section: PHP
      option: allow_url_include
      value: "On"
    - section: PHP
      option: error_reporting
      value: E_ALL

ose:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-ose-4.4.0-SNAPSHOT.noarch.rpm"
  dbseed: "https://nexus.dynacommercelab.com/repository/db-seeds/vfde/omnius-ose-4.4.0.sql.gz"
  mysql:
    host: "{{ hosts.mysql }}"
    port: 3306
    db: "ose_st6"
    username: "ose_st6"
    password: "{{ vault.ose.mysql.password }}"
    encoding: "utf8"
  redis_session:
    host: "{{ hosts.redis }}"
    password: "{{ vault.ose.redis_session.password }}"
    port: 6379
    db: "{{ redis.ose.redis_session }}"
    log_level: 10
    min_lifetime: 1800 # seconds
    max_lifetime: 1800 # seconds
  backend_options:
    server: "{{ hosts.redis }}"
    password: "{{ vault.ose.backend_options.password }}"
    port: 6379
    database: "{{ redis.ose.backend_options }}"
  logging:
    redis:
      active: 0
      host: "{{ hosts.redis }}"
      password: "{{ vault.ose.logging.redis.password }}"
      database:  "{{ redis.ose.logging }}"
      port: 6379
      keyPrefix: omnius-ose
      stream: default
    stream:
      active: 1
      loglevel: DEBUG
  vfde:
    frontname: implement
    redis_loglevel: DEBUG
    filePermission: "0744"
    
  storeviews:
    stores:
      - name: telesales
        base_url: https://st6-telesales-vfde.dynacommercelab.com/
        scope_id: 0
  configuration:
    - key: web/unsecure/base_url
      value: https://st6-telesales-vfde.dynacommercelab.com/
    - key: web/secure/base_url
      value: https://st6-telesales-vfde.dynacommercelab.com/
    - key: omnius_service/job_queue/amqp_user
      value: dyna.ose
    - key: omnius_service/job_queue/amqp_pass
      value: "{{ vault.rabbitmq.ose }}"
    - key: omnius_service/job_queue/amqp_host
      value: "{{ hosts.rabbitmq }}"
    - key: omnius_service/job_queue/amqp_vhost
      value: st6
    - key: omnius_service/job_queue/amqp_port
      value: 5672
    - key: omnius_service/job_queue/amqp_ssl
      value: 0
    - key: omnius_service/job_queue/amqpp_verify_peer
      value: 0
    - key: omnius_service/job_queue/amqp_allow_self_signed
      value: 1
    - key: omnius_service/job_queue/amqp_exchange
      value: dyna.ose.jobs.direct
    - key: omnius_service/job_queue/amqp_queue
      value: dyna.ose.jobs
    - key: omnius_service/job_queue/amqp_retry_queue
      value: dyna.ose.retry
    - key: omnius_service/job_queue/amqp_seconds_between_retries
      value: 300
    - key: omnius_service/job_queue/amqp_retries
      value: 5
    - key: omnius_service/reportingqueueoptions/reporting_host
      value: "{{ hosts.rabbitmq }}"
    - key: omnius_service/reportingqueueoptions/reporting_port
      value: 5672
    - key: omnius_service/reportingqueueoptions/reporting_user
      value: dyna.ose
    - key: omnius_service/reportingqueueoptions/reporting_password
      value: "{{ vault.rabbitmq.ose }}"
    - key: omnius_service/reportingqueueoptions/reporting_vhost
      value: st6
    - key: omnius_service/reportingqueueoptions/reporting_exchange
      value: dyna.ose.direct
    - key: omnius_service/reportingqueueoptions/reporting_queue
      value: amdocs.ogw.quotes
    - key: omnius_service/reportingqueueoptions/reporting_routingkey
      value: amdocs.ogw.quotes
    - key: omnius_service/workitemqueueoptions/workitem_host
      value: "{{ hosts.rabbitmq }}"
    - key: omnius_service/workitemqueueoptions/workitem_port
      value: 5672
    - key: omnius_service/workitemqueueoptions/workitem_user
      value: dyna.ose
    - key: omnius_service/workitemqueueoptions/workitem_password
      value: "{{ vault.rabbitmq.ose }}"
    - key: omnius_service/workitemqueueoptions/workitem_vhost
      value: st6
    - key: omnius_service/workitemqueueoptions/workitem_exchange
      value: dyna.ose.direct
    - key: omnius_service/workitemqueueoptions/workitem_queue
      value: dyna.oil.workitems
    - key: omnius_service/workitemqueueoptions/workitem_routingkey
      value: Dyna.Oil.Workitems
    - key: omnius_service/process_order/use_amqp
      value: 1
  phpfpm:
    pools:
      - name: omnius-ose
        settings:
          - option: user
            value: omnius-ose
          - option: group
            value: omnius-ose
          - option: listen
            value: /run/php-fpm/omnius-ose-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  rabbitmq:
    vhost: st6
    exchange:
      - dyna.oil.dlx
      - dyna.ose.direct
      - dyna.ose.dlx
      - amdocs.ogw.dlx
      - amdocs.ogw.direct
      - dyna.oil.direct
      - dyna.ose.jobs.direct
      - dyna.ose.setstatus
      - jobs_exchange
      - dyna.ose.headers
    queue_args:
      - name: amdocs.ogw.pushcustomerdata
        exchange: "amdocs.ogw.dlx"
      - name: amdocs.ogw.customerupdate.dlq
      - name: dyna.oil.saporders.archive
        ttl: 86400000
      - name: amdocs.ogw.quotes
      - name: amdocs.ogw.pushorderstatus.dlq
        exchange: "amdocs.ogw.dlx"
      - name: amdocs.ogw.quotes.dlq
      - name: dyna.oil.workitems
        exchange: "dyna.oil.dlx"
      - name: TestQueue
      - name: dyna.oil.saporders
        exchange: "dyna.oil.dlx"
      - name: dyna.oil.workitems.dlq
      - name: amdocs.ogw.customerupdate
        exchange: "amdocs.ogw.dlx"
      - name: dyna.ose.customerupdate.dlq
      - name: TestPushOrderSTatus
      - name: dyna.ose.jobs
      - name: dyna.ose.saporders
        exchange: "dyna.ose.dlx"
      - name: dyna.ose.retry
        exchange: "dyna.ose.jobs.direct"
        routing: "dyna.ose.jobs"
        ttl: 20000
      - name: dyna.oil.saporders.dlq
      - name: amdocs.ogw.pushcustomerdata.dlq
      - name: dyna.ose.delayqueue
        exchange: jobs_exchange
        routing: dyna.ose.jobs
        ttl: 10000
      - name: dyna.ose.setstatus
        exchange: dyna.ose.direct
      - name: dyna.ose.customerupdate
        exchange: "dyna.ose.dlx"
      - name: dyna.ose.pushorderstatus.dlq
      - name: dyna.ose.saporders.dlq
      - name: dyna.ose.pushorderstatus
        exchange: "dyna.ose.dlx"
      - name: amdocs.ogw.pushorderstatus
        exchange: "amdocs.ogw.dlx"

    bindings:
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.customerupdate
        destination_type: queue
        routing_key: amdocs.ogw.customerupdate
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.pushcustomerdata
        destination_type: queue
        routing_key: amdocs.ogw.pushcustomerdata
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.pushorderstatus
        destination_type: queue
        routing_key: amdocs.ogw.pushorderstatus
      - name: amdocs.ogw.direct
        destination: amdocs.ogw.quotes
        destination_type: queue
        routing_key: amdocs.ogw.quotes
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.customerupdate.dlq
        destination_type: queue
        routing_key: amdocs.ogw.customerupdate
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.customerupdate.dlq
        destination_type: queue
        routing_key: amdocs.ogw.pushcustomerdata
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.customerupdate.dlq
        destination_type: queue
        routing_key: amdocs.ogw.pushorderstatus
      - name: amdocs.ogw.dlx
        destination: amdocs.ogw.quotes.dlq
        destination_type: queue
        routing_key: amdocs.ogw.quotes
      - name: dyna.oil.direct
        destination: dyna.oil.workitems
        destination_type: queue
        routing_key: dyna.oil.workitems
      - name: dyna.oil.direct
        destination: dyna.ose.saporders
        destination_type: queue
        routing_key: dyna.ose.saporders
      - name: dyna.oil.direct
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: dyna.ose.setstatus
      - name: dyna.oil.dlx
        destination: dyna.oil.saporders.dlq
        destination_type: queue
        routing_key: dyna.oil.saporders
      - name: dyna.oil.dlx
        destination: dyna.oil.workitems.dlq
        destination_type: queue
        routing_key: dyna.oil.workitems
      - name: dyna.ose.direct
        destination: amdocs.ogw.quotes
        destination_type: queue
        routing_key: amdocs.ogw.quotes
      - name: dyna.ose.direct
        destination: dyna.oil.saporders
        destination_type: queue
        routing_key: dyna.oil.saporders
      - name: dyna.ose.direct
        destination: dyna.oil.saporders.archive
        destination_type: queue
        routing_key: dyna.oil.saporders
      - name: dyna.ose.direct
        destination: dyna.oil.workitems
        destination_type: queue
        routing_key: dyna.oil.workitems
      - name: dyna.ose.direct
        destination: dyna.ose.saporders
        destination_type: queue
        routing_key: dyna.ose.saporders
      - name: dyna.ose.direct
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: dyna.ose.setstatus
      - name: dyna.ose.dlx
        destination: dyna.ose.customerupdate.dlq
        destination_type: queue
        routing_key: dyna.ose.customerupdate
      - name: dyna.ose.dlx
        destination: dyna.ose.pushorderstatus.dlq
        destination_type: queue
        routing_key: dyna.ose.pushorderstatus
      - name: dyna.ose.dlx
        destination: dyna.ose.saporders.dlq
        destination_type: queue
        routing_key: dyna.ose.saporders
      - name: dyna.ose.headers
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: dyna.ose.setstatus
      - name: dyna.ose.jobs.direct
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: ""
      - name: dyna.ose.jobs.direct
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: dyna.ose.jobs
      - name: dyna.ose.jobs.direct
        destination: dyna.ose.retry
        destination_type: queue
        routing_key: dyna.ose.retry
      - name: dyna.ose.setstatus
        destination: dyna.ose.setstatus
        destination_type: queue
        routing_key: ""
      - name: jobs_exchange
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: ""
      - name: jobs_exchange
        destination: dyna.ose.delayqueue
        destination_type: queue
        routing_key: dyna.ose.delayqueue
      - name: jobs_exchange
        destination: dyna.ose.jobs
        destination_type: queue
        routing_key: dyna.ose.jobs

vfde_dbextract:
  mysql:
    db: "dbextract"
    encoding: "utf8"
    dbseed: "https://nexus.dynacommercelab.com/repository/db-seeds/vfde/omnius-ose-dbextract-4.7.0-SNAPSHOT.sql.gz"

jobdaemon:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/jobdaemon/omnius-jobdaemon-1.1-SNAPSHOT.noarch.rpm
  rabbitmq_password: "{{ vault.rabbitmq.oil }}"
  influxdb_password: "{{ vault.jobdaemon.influxdb_password }}"
  config:
    php_exec_path: /usr/bin/php
    php_script: /opt/omnius/ose/shell/job_processor_runner.php
    max_threads: 50
    rabbitmq:
      username: dyna.oil
      hostname: "{{ hosts.rabbitmq }}"
      vhost: st6
      port: 5672
      queue_job: dyna.ose.jobs
      queue_retry_exchange: dyna.ose.jobs.direct
      queue_retry: dyna.ose.retry
      ssl:
        enabled: "false"
        accept_policy_errors: "false"
        skip_validation: "false"
      max_retries: 10
      receive_timeout: 1000
      message_interval: 10
      wait_interval: 0
      max_messages: 100
    influxdb:
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: "vfdejobdaemon"
      database: "vfdejobdaemon"
      inflow: jobdaemon_inflow
      delay: jobdaemon_delay
      ack: jobdaemon_ack
    log_path: /var/log/omnius/jobdaemon/
    redis:
      host: "{{ hosts.redis }}"
      port: 6379
      expire: 3600
      db: "{{ redis.jobdaemon }}"

statusdaemon:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/statusdaemon/omnius-statusdaemon-1.1.0-1.noarch.rpm
  config:
    php_script: /opt/omnius/ose/shell/amqp/consumer.php
    rabbitmq:
      username: dyna.ose
      password: "{{ vault.rabbitmq.ose }}"
      hostname: "{{ hosts.rabbitmq }}"
      vhost: st6
      port: 5672
      queue: dyna.ose.setstatus
      receive_timeout: 1000
      message_interval: 10
      wait_internal: 0
      max_messages: 100
    influxdb:
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: vfdestatusdaemon
      password: "{{ vault.statusdaemon.influxdb_password }}"
      database: vfdestatusdaemon
      inflow: statusdaemon_inflow
      delay: statusdaemon_delay
    log_path: /var/log/omnius/statusdaemon/
    redis:
      host: "{{ hosts.redis }}"
      port: 6379
      expire: 3600
      db: "{{ redis.statusdaemon }}"

sapdaemon:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-sapdaemon-1.4-SNAPSHOT.noarch.rpm"
  config:
    mysql:
      host: "{{ hosts.mysql }}"
      username: "{{ ose.mysql.username }}"
      password: "{{ vault.ose.mysql.password }}"
      db: "{{ ose.mysql.db }}"
    ose:
      host: "{{ hosts.rabbitmq }}"
      port: 5672
      vhost: st6
      name: dyna.ose.saporders
      username: dyna.ose
      password: "{{ vault.rabbitmq.ose }}"
      ssl: "false"
      ssl_accept_policy_errors: "false"
      ssl_skip_validation: "true"
    move_immediately: "false"
    move_polling_times: 5
    max_messages: 250
    logging:
      extra: "true"
      path: "/var/log/omnius/sapdaemon/"
      maxsize: 10240
    delete_invalid_after_days: 30
    processorder_only: "false"
    add_xml_declaration: "true"
    statusses_to_send: OnHold-PendingHWFulfillment
    influxdb:
      enabled: "true"
      host: https://telegraf-influxdb.dynacommercelab.com
      port: 443
      username: "vfdesapdaemon"
      password: "{{ vault.sapdaemon.influxdb_password }}"
      database: "vfdesapdaemon"
      inflow: sapdaemon_inflow
      delay: sapdaemon_delay
      ack: "sapdaemon_ack"

vfde_uct:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-uct-api-4.0-SNAPSHOT.noarch.rpm
  phpfpm:
    pools:
      - name: omnius-uct
        settings:
          - option: user
            value: omnius-uct
          - option: group
            value: omnius-uct
          - option: listen
            value: /run/php-fpm/omnius-uct-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: st6
    - section: null
      option: APP_DEBUG
      value: true
    - section: null
      option: APP_KEY
      value: base64:SmVXKPvMl3lxk3KssOU3V8LvZ8v5JTMITSarlY7jx4c=
    - section: null
      option: DB_CONNECTION
      value: mysql
    - section: null
      option: DB_HOST
      value: "{{ hosts.mysql }}"
    - section: null
      option: DB_PORT
      value: "{{ ose.mysql.port }}"
    - section: null
      option: DB_DATABASE
      value: "{{ ose.mysql.db }}"
    - section: null
      option: DB_USERNAME
      value: "{{ ose.mysql.username }}"
    - section: null
      option: DB_PASSWORD
      value: "{{ vault.ose.mysql.password }}"
    - section: null
      option: REDIS_CLUSTER
      value: false
    - section: null
      option: REDIS_HOST
      value: "{{ hosts.redis }}"
    - section: null
      option: REDIS_PORT
      value: "{{ ose.redis_session.port }}"
    - section: null
      option: REDIS_DATABASE
      value: "{{ redis.vfde_uct }}"
    - section: null
      option: REDIS_PASSWORD
      value: "{{ vault.ose.redis_session.password }}"
    - section: null
      option: CACHE_DRIVER
      value: memcached
    - section: null
      option: QUEUE_DRIVER
      value: sync
    - section: null
      option: DIGEST_REALM
      value: uct@st6-telesales-vfde.dynacommercelab.com
    - section: null
      option: DIGEST_USER
      value: dyna-uct
    - section: null
      option: DIGEST_PASS
      value: Dyna123
    - section: null
      option: DOMAIN_URL_INTERNAL
      value: https://st6-telesales-vfde.dynacommercelab.com/uct/
    - section: null
      option: API_URL_INTERNAL
      value: uct
    - section: null
      option: DOMAIN_URL_EXTERNAL
      value: https://st6-telesales-vfde.dynacommercelab.com/uct/
    - section: null
      option: API_URL_EXTERNAL
      value: uct
    - section: null
      option: DOMAIN_URL
      value: https://st6-telesales-vfde.dynacommercelab.com/uct/

vfde_um:
  package: https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-um-api-4.0-SNAPSHOT.noarch.rpm
  phpfpm:
    pools:
      - name: omnius-um
        settings:
          - option: user
            value: omnius-um
          - option: group
            value: omnius-um
          - option: listen
            value: /run/php-fpm/omnius-um-php-fpm.sock
          - option: listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: st6
    - section: null
      option: APP_DEBUG
      value: true
    - section: null
      option: APP_KEY
      value: base64:SmVXKPvMl3lxk3KssOU3V8LvZ8v5JTMITSarlY7jx4c=
    - section: null
      option: DEFAULT_PASS
      value: secret
    - section: null
      option: DB_CONNECTION
      value: mysql
    - section: null
      option: DB_HOST
      value: "{{ hosts.mysql }}"
    - section: null
      option: DB_PORT
      value: "{{ ose.mysql.port }}"
    - section: null
      option: DB_DATABASE
      value: "{{ ose.mysql.db }}"
    - section: null
      option: DB_USERNAME
      value: "{{ ose.mysql.username }}"
    - section: null
      option: DB_PASSWORD
      value: "{{ vault.ose.mysql.password }}"
    - section: null
      option: CACHE_DRIVER
      value: memcached
    - section: null
      option: QUEUE_DRIVER
      value: sync
    - section: null
      option: DIGEST_REALM
      value: usermanagement@st6-um-vfde.dynacommercelab.com
    - section: null
      option: DIGEST_USER
      value: dyna-um
    - section: null
      option: DIGEST_PASS
      value: Dyna123
    - section: null
      option: MAGENTO_PASSWORD_SALT
      value: salt
    - section: null
      option: DEFAULT_STORE_ID
      value: 2
    - section: null
      option: DEFAULT_ROLE_ID
      value: 1
    - section: null
      option: DEFAULT_DEALER_ID
      value: 1
    - section: null
      option: DEFAULT_IS_ACTIVE
      value: 1
    - section: null
      option: DEFAULT_TEMPORARY_PASSWORD
      value: 1
    - section: null
      option: REDIS_CLUSTER
      value: false
    - section: null
      option: REDIS_HOST
      value: "{{ hosts.redis }}"
    - section: null
      option: REDIS_PORT
      value: "{{ ose.redis_session.port }}"
    - section: null
      option: REDIS_DATABASE
      value: "{{ redis.vfde_um }}"
    - section: null
      option: REDIS_PASSWORD
      value: "{{ vault.ose.redis_session.password }}"

sales_api:
  package: "https://nexus.dynacommercelab.com/repository/rpm-packages/vfde/omnius-rest-api-1.0.5-SNAPSHOT.noarch.rpm"
  phpfpm:
    pools:
      - name: omnius-sales-api
        settings:
          - option: user
            value: omnius-restapi
          - option: group
            value: omnius-restapi
          - option: listen
            value: /run/php-fpm/omnius-sales-api-php-fpm.sock
          - option : listen.owner
            value: nginx
          - option: listen.group
            value: nginx
  config:
    - section: null
      option: APP_ENV
      value: dev
    - section: null
      option: APP_DEBUG
      value: true
    - section: null
      option: APP_KEY
      value: base64:SmVXKPvMl3lxk3KssOU3V8LvZ8v5JTMITSarlY7jx4c=
    - section: null
      option: DB_CONNECTION
      value: sqlite
    - section: null
      option: CACHE_DRIVER
      value: redis
    - section: null
      option: SESSION_DRIVER
      value: cookie
    - section: null
      option: MAIL_DRIVER
      value: smtp
    - section: null
      option: STUBS_PATH
      value: /opt/omnius/ose/var/stubs/
    - section: null
      option: MAGENTO_STORE
      value: 2
    - section: null
      option: MAGENTO_BASE_PATH
      value: /opt/omnius/ose/
    - section: null
      option: REDIS_HOST
      value: "{{ hosts.redis }}"
    - section: null
      option: REDIS_PORT
      value: 6379
    - section: null
      option: REDIS_PASSWORD
      value: "{{ vault.ose.redis_session.password }}"
    - section: null
      option: REDIS_DATABASE
      value: "{{ redis.sales_api }}"
    - section: null
      option: TAG_BAN
      value: "ban:"
    - section: null
      option: TAG_CUSTOMER_NO
      value: "customer:"
    - section: null
      option: TAG_OFFERS
      value: "offers:"
    - section: null
      option: TAG_JOBS
      value: "jobs:"
    - section: null
      option: STATUS_OFFER_SUBMITTED
      value: "Submitted"
    - section: null
      option: STATUS_OFFER_EXPIRED
      value: "OutOfDate"
    - section: null
      option: STATUS_OFFER_VALID
      value: "Valid"
    - section: null
      option: AMQP_HOST
      value: "{{ hosts.rabbitmq }}"
    - section: null
      option: AMQP_PORT
      value: 5672
    - section: null
      option: AMQP_USER
      value: dyna.oil
    - section: null
      option: AMQP_PASS
      value: "{{ vault.rabbitmq.oil }}"
    - section: null
      option: AMQP_VHOST
      value: st6
    - section: null
      option: AMQP_EXCHANGE
      value: dyna.ose.jobs.direct
    - section: null
      option: AMQP_QUEUE_NAME
      value: dyna.ose.jobs
    - section: null
      option: AMQP_CONSUMER_TAG
      value: consumer
    - section: null
      option: BASIC_USERNAME
      value: dyna-restapi
    - section: null
      option: BASIC_PASSWORD
      value: Dyna123
    - section: null
      option: LOG_PATH
      value: /var/log/omnius/restapi/lumen.log
