#!/bin/bash

KOPS_HOME=$HOME
KUBE_HOME=$KOPS_HOME/kubectl
SUBDOMAIN_NAME=
KUBE_CONFIG_HOME=$KOPS_HOME/.kube/config
HOSTED_ZONE_FILE=$KOPS_HOME/hosted-zone.json
K8_SUB_DOMAIN_DEFAULT=$PWD/k8-sub-domain-default.json
K8_SUB_DOMAIN_ENV=$KOPS_HOME/k8-sub-domain.json
AWS_REGION="eu-central-1"

getSubDomain() {
read -p "Please specify a cluster name : " SUBDOMAIN_NAME
if [[ $SUBDOMAIN_NAME == "" ]]; then
    echo "Exiting...cluster name is MANDATORY"
    exit
fi
}

installKubectl() {
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.11.2/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
}

installKops() {
wget https://github.com/kubernetes/kops/releases/download/1.10.0/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/local/bin/kops
}

createS3Bucket() {
aws s3 rb s3://$SUBDOMAIN_NAME-kubernetes-state --region $AWS_REGION
aws s3 mb s3://$SUBDOMAIN_NAME-kubernetes-state --region $AWS_REGION
export KOPS_STATE_STORE=s3://$SUBDOMAIN_NAME-kubernetes-state
}

createSSHKeys() {
touch $KUBE_CONFIG_HOME
export KUBECONFIG="$KUBE_CONFIG_HOME"
echo "KUBECONFIG=$KUBECONFIG"
ssh-keygen -f $SUBDOMAIN_NAME -t rsa -N ''
}

createSubDomain() {
rm -rf $HOSTED_ZONE_FILE
ID=$(uuidgen) && aws route53 create-hosted-zone --name $SUBDOMAIN_NAME --caller-reference $ID >>$HOSTED_ZONE_FILE
}

createComment() {
COMMENT=$1
jq '. | .Comment="'"$COMMENT"'"' $K8_SUB_DOMAIN_DEFAULT >>$K8_SUB_DOMAIN_ENV
}

createResourceRecordSet() {
SUBDOMAIN_NAME=$1
jq '. | .Changes[0].ResourceRecordSet.Name="'"$SUBDOMAIN_NAME"'"' $K8_SUB_DOMAIN_ENV >>$KOPS_HOME/k8-sub-domain-updated.json
mv $KOPS_HOME/k8-sub-domain-updated.json $K8_SUB_DOMAIN_ENV
createAddress
}

createAddress() {
ADDRESS_1=$(jq '. | .DelegationSet.NameServers[0]' $KOPS_HOME/hosted-zone.json)
ADDRESS_2=$(jq '. | .DelegationSet.NameServers[1]' $KOPS_HOME/hosted-zone.json)
ADDRESS_3=$(jq '. | .DelegationSet.NameServers[2]' $KOPS_HOME/hosted-zone.json)
ADDRESS_4=$(jq '. | .DelegationSet.NameServers[3]' $KOPS_HOME/hosted-zone.json)
jq '. | .Changes[0].ResourceRecordSet.ResourceRecords[0].Value='"$ADDRESS_1"' | .Changes[0].ResourceRecordSet.ResourceRecords[1].Value='"$ADDRESS_2"' | .Changes[0].ResourceRecordSet.ResourceRecords[2].Value='"$ADDRESS_3"' | .Changes[0].ResourceRecordSet.ResourceRecords[3].Value='"$ADDRESS_4"' ' $K8_SUB_DOMAIN_ENV >>$KOPS_HOME/k8-sub-domain-updated.json
mv $KOPS_HOME/k8-sub-domain-updated.json $K8_SUB_DOMAIN_ENV
}

createRecordInParentDomain() {
PARENT_HOSTED_ZONE_ID=$(aws route53 list-hosted-zones | jq --raw-output '. | .HostedZones[0].Id')
CHANGE_ID=$(aws route53 change-resource-record-sets \
    --hosted-zone-id $PARENT_HOSTED_ZONE_ID \
    --change-batch file://$KOPS_HOME/k8-sub-domain.json | jq --raw-output '. | .ChangeInfo.Id')
CHANGE_STATUS="PENDING"
while [[ $CHANGE_STATUS == "PENDING" ]]; do
    sleep 60s
    CHANGE_STATUS=$(aws route53 get-change --id $CHANGE_ID | jq --raw-output '. | .ChangeInfo.Status')
done
createCluster
}

createCluster() {
createSSHKeys
createS3Bucket
SSH_PUBLIC_KEY=${SUBDOMAIN_NAME}.pub
kops create cluster \
    --cloud=aws \
    --image ami-862140e9 \
    --node-count 4 \
    --master-size=t2.xlarge \
    --zones ${AWS_REGION}a \
    --name=$SUBDOMAIN_NAME \
    --node-size=t2.xlarge \
    --ssh-public-key=$SSH_PUBLIC_KEY \
    --admin-access=93.117.239.90/32 --admin-access=14.143.133.197/32 --admin-access=18.185.219.172/32 --admin-access=85.147.250.89/32 \
    --cloud-labels "Project=dclab,Role=cluster,Environment=infra" \
    --dns-zone $SUBDOMAIN_NAME
kops update cluster $SUBDOMAIN_NAME --yes
while true; do
    kops validate cluster $SUBDOMAIN_NAME && break || sleep 60s
done
}

installHelm() {
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
kubectl create -f helm-rbac-config.yaml
helm init --service-account tiller
sleep 30s
}

deployExternalDNS() {
export KUBE_CONFIG_HOME
NODE_ROLE="nodes.${SUBDOMAIN_NAME}"
aws iam put-role-policy --role-name ${NODE_ROLE} --policy-name external-dns-policy --policy-document '{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Effect": "Allow",
     "Action": [
       "route53:ChangeResourceRecordSets"
     ],
     "Resource": [
       "arn:aws:route53:::hostedzone/*"
     ]
   },
   {
     "Effect": "Allow",
     "Action": [
       "route53:ListHostedZones",
       "route53:ListResourceRecordSets"
     ],
     "Resource": [
       "*"
     ]
   }
 ]
}'
helm install stable/external-dns --name=external-dns --set policy=sync --set rbac.create=true
}

installIngress() {
kubectl apply -f nginx-ingress-controller.yaml
}

installKubewatch() {
helm install --name kubewatch stable/kubewatch -f kubewatch-values.yaml
}

installMetrics-Server() {
	kubectl apply -f metrics-server.yaml
}

getClusterConfig() {
kops get cluster --name=$SUBDOMAIN_NAME --output yaml > cluster.yml && echo -e "\n---\n" >> cluster.yml && kops get instancegroups --name=$SUBDOMAIN_NAME --output yaml >> cluster.yml
}
echo "#########################"
echo "Starting a clean INSTALL."
    getSubDomain
    installKops
    installKubectl
    createSubDomain
    createComment "k8 subdomain $SUBDOMAIN_NAME"
    createResourceRecordSet "$SUBDOMAIN_NAME"
    createRecordInParentDomain
    installHelm
    deployExternalDNS
    installIngress
    installKubewatch
    installMetrics-Server
    getClusterConfig
echo "     Finished!     "
echo "#########################"
