Kops, or Kubernetes Operations is a command line tool to provision a Kubernetes cluster on AWS.

### Prerequisites
- AWS Command Line
- jq
- The IAM user to create the Kubernetes cluster must have the following permissions:
  -  AmazonEC2FullAccess
  -  AmazonRoute53FullAccess
  -  AmazonS3FullAccess
  -  IAMFullAccess
  -  AmazonVPCFullAccess
- A Valid domain


### Creating the Cluster
```
chmod +x Install-Kubernetes.sh
./Install-Kubernetes.sh
```

Certain addons are also installed into newly created clusters

- Heapster
- Kubernetes Dashboard
- Kubernetes ExternalDNS
- NGINX Ingress Controller

After Kops deployment has finished please store private key `<CLUSTER_NAME>`, public key `<CLUSTER_NAME>.pub` and kubeconfig file `~/.kube/config` in KeePass and `cluster.yml` file in git.

ssh to a particular kube node, download private key file from keypass and execute
`$ ssh -i <path of the private key file> core@<ip of the aws kube instances>`

 To access HTTPS endpoint of dashboard go to: `https://api.<CLUSTER_NAME>/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/`

The login credentials are: Username: `admin`

Password: get by running `kops get secrets kube --type secret -oplaintext`

Retrieve an authentication token for the `dashboard` service account and use this token to connect to the dashboard.

`$ kubectl get secret $(kubectl get serviceaccount dashboard -o jsonpath="{.secrets[0].name}") -o jsonpath="{.data.token}" | base64 --decode`


### Controlling the cluster
This guide assumes you have:

- A working Kubernetes Cluster built with kops
- AWS CLI configured
- Kubernetes CLI utilities installed - kops (1.10.0) + kubectl (1.11.2)

Set KOPS_STATE_STORE to point to the S3 bucket:

`$ export KOPS_STATE_STORE=s3://<CLUSTER_NAME>-kubernetes-state`

Open cluster.yml file

- ssh access to cluster
Jump down to the `sshAccess` and add IP address to access your cluster by using SSH

`- 12.34.56.78/32`

- Kubernetes API access

Jump down to the `kubernetesApiAccess` section and add IP address to access your cluster

`- 12.34.56.78/32`

- add new nodes to the existing Kubernetes cluster online

Find maxSize/minSize parameter and change it to desire new size of the cluster


After save the configuration changes in git, you can use the following command to deploy the changes

`$ kops replace -f cluster.yml`

Above command replaced the cluster settings. To confirm the change you have to run the update command to apply the change.

`$ kops update cluster --yes`
