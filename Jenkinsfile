#!groovy

pipeline {
    agent { node { label 'linux' } }

    options {
        timeout(time: 15, unit: 'MINUTES')
        buildDiscarder(logRotator(numToKeepStr: '5'))
        disableConcurrentBuilds()
    }

    environment {
        DEPLOY_CRED = credentials('AnsibleVault')
        NEXUS_URL = 'https://nexus.dynacommercelab.com'
        channel = 'aws-alerts'
    }

    parameters {
        choice(
            name: 'env',
            description: 'Which environment do you want to deploy?',
            choices: 'baseline-st1\ncaiway-st1\ndelta-st1\ndelta-st2\nvfde-st1\nvfde-st2\nvfde-st3\nvfde-st4\nvfde-st5\nvfde-st6\nvfde-st7\nvfde-st8\nvfde-st9\nvfde-st10\nvfde-st11\nvznl-st1\nvznl-st2\nvznl-st3\nvznl-st4\nvznl-st5\nvznl-st6\nvznl-st7\nvznl-st8\ndemo-web1'
        )
        choice(
            name: 'component',
            description: 'Which component type are you deploying?',
            choices: 'ose\nserviceability\nserviceability_adapter\nvalidateaddress\nvalidateaddress_adapter\njobdaemon\nstatusdaemon\nvfde-sapdaemon\ncoc_api\ncoc_cli\nvfde_uct\nvfde_um\ncustomersearch_api\ncustomersearch_indexer\ncustomersearch_adapter\nose_catalog\nose_order\nose_order_schema\nose_shoppingcart\nose_shoppingcart_schema\ncheckout\nmultimapper\ninstalledbase\ninstalledbase_adapter\nsales_api'
        )
        choice(
            name: 'packageType',
            description: 'Which package type are you deploying?',
            choices: 'rpm'
        )
        string(
            name: 'package',
            description: 'Which package do you want to deploy?',
            defaultValue: 'baseline/omnius-ose-2.2-SNAPSHOT.noarch.rpm'
        )
        booleanParam(
            name: 'resetDatabase',
            description: 'Do you want to reset the database?',
            defaultValue: false
        )
        string(
            name: 'slackChannel',
            description: 'Where do you want to get notified?',
            defaultValue: ''
        )
    }

    stages {
        stage('Deploy') {
            when {
                allOf {
                    buildingTag()
                    expression { params.env != ""}
                }
            }
            steps {
                script {
                    switch (params.component) {
                        case 'ose':
                            env.extravars = "--extra-vars '{ \"ose\": { \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }'";
                            break;
                        case 'coc_api':
                            env.extravars = "--extra-vars '{ \"coc\":{ \"api\": { \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } } }\'";
                            break;
                        case 'coc_cli':
                            env.extravars = "--extra-vars '{ \"coc\":{ \"cli\": { \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } } }\'";
                            break;
                        case 'statusdaemon':
                            env.extravars = "--extra-vars '{ \"statusdaemon\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'vfde_uct':
                            env.extravars = "--extra-vars '{ \"vfde_uct\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'vfde_um':
                            env.extravars = "--extra-vars '{ \"vfde_um\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'customersearch_api':
                            env.extravars = "--extra-vars '{ \"customersearch\":{ \"api\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } } } '";
                            break;
                        case 'customersearch_indexer':
                            env.extravars = "--extra-vars '{ \"customersearch\":{ \"indexer\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } } } '";
                            break;
                        case 'customersearch_adapter':
                            env.extravars = "--extra-vars '{ \"customersearch_adapter\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } '";
                            break;
                        case 'ose_catalog':
                            env.extravars = "--extra-vars '{ \"ose_catalog\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'ose_order':
                            env.extravars = "--extra-vars '{ \"ose_order\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'ose_order_schema':
                            env.extravars = "--extra-vars '{ \"ose_order\":{ \"schema\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'ose_shoppingcart':
                            env.extravars = "--extra-vars '{ \"ose_shoppingcart\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'ose_shoppingcart_schema':
                            env.extravars = "--extra-vars '{ \"ose_shoppingcart\":{ \"schema\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'checkout':
                            env.extravars = "--extra-vars '{ \"checkout\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'multimapper':
                            env.extravars = "--extra-vars '{ \"multimapper\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'installedbase':
                            env.extravars = "--extra-vars '{ \"installedbase\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'installedbase_adapter':
                            env.extravars = "--extra-vars '{ \"installedbase\":{ \"adapter\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'serviceability':
                            env.extravars = "--extra-vars '{ \"serviceability\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'serviceability_adapter':
                            env.extravars = "--extra-vars '{ \"serviceability\":{ \"adapter\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'validateaddress':
                            env.extravars = "--extra-vars '{ \"validateaddress\":{ \"package\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        case 'validateaddress_adapter':
                            env.extravars = "--extra-vars '{ \"validateaddress\":{ \"adapter\": \"${env.NEXUS_URL}/repository/${params.packageType}-packages/${params.package}\" } }\'";
                            break;
                        default:
                            env.extravars = '';
                    }
                }
                echo "Deploying ${params.component} ${params.package} to environment ${params.env}"
                withCredentials([sshUserPrivateKey(credentialsId: 'AwsCentosPem', keyFileVariable: 'AWS_CENTOS_PEM')]) {
                    sh """
                        export ANSIBLE_CONFIG=ansible/ansible.cfg
                        export ANSIBLE_FORCE_COLOR=true
                        echo \"${env.DEPLOY_CRED}\" > .vault_password
                        ansible-playbook -i ansible/inventories/staging/${params.env}/hosts \
                            --vault-password-file .vault_password \
                            ${env.extravars} \
                            --private-key ${env.AWS_CENTOS_PEM} \
                            ansible/playbooks/maintenance/update_${params.component}.yml -vv
                        rm -rf .vault_password
                    """

                    sh """
                        export ANSIBLE_CONFIG=ansible/ansible.cfg
                        export ANSIBLE_FORCE_COLOR=true
                        echo \"${env.DEPLOY_CRED}\" > .vault_password
                        ansible-playbook -i ansible/inventories/staging/${params.env}/hosts \
                            --vault-password-file .vault_password \
                            ${env.extravars} \
                            --tags ${params.env} \
                            --private-key ${env.AWS_CENTOS_PEM} \
                            ansible/playbooks/maintenance/update_${params.component}.yml -vv
                        rm -rf .vault_password
                    """
                }
            }
        }

        stage('Reset database') {
            when {
                allOf {
                    buildingTag()
                    expression { params.component ==~ /ose|checkout|multimapper|customersearch.*/ && params.resetDatabase && params.env != "" }
                }
            }
            steps {
                script {
                    component = params.component;
                    switch (params.component) {
                        case 'ose':
                            dbseed = params.package.replace(".noarch.${params.packageType}", '.sql.gz')
                            env.extravars = "--extra-vars '{ \"ose\": { \"dbseed\": \"${env.NEXUS_URL}/repository/db-seeds/${dbseed}\" } }'";
                            break;
                        case 'customersearch_api':
                        case 'customersearch_indexer':
                            component = "customersearch"
                            env.extravars = '';
                            break;
                        case 'multimapper':
                            dbseed = params.package.replace(".noarch.${params.packageType}", '.sql.gz')
                            env.extravars = "--extra-vars '{ \"multimapper\": { \"dbseed\": \"${env.NEXUS_URL}/repository/db-seeds/${dbseed}\" } }'";
                            break;
                        default:
                            env.extravars = '';
                    }
                }
                echo "Reset database using '${env.extravars}' on environment ${params.env}"
                withCredentials([sshUserPrivateKey(credentialsId: 'AwsCentosPem', keyFileVariable: 'AWS_CENTOS_PEM')]) {
                    sh """
                        export ANSIBLE_CONFIG=ansible/ansible.cfg
                        export ANSIBLE_FORCE_COLOR=true
                        echo \"${env.DEPLOY_CRED}\" > .vault_password
                        ansible-playbook -i ansible/inventories/staging/${params.env}/hosts \
                            --vault-password-file .vault_password \
                            ${env.extravars} \
                            --private-key ${env.AWS_CENTOS_PEM} \
                            ansible/playbooks/maintenance/reset_${component}_database.yml -vv
                        rm -rf .vault_password
                    """
                }
            }
        }

        stage('Verify deployment') {
            when {
                allOf {
                    buildingTag()
                    expression { params.env != ""}
                }
            }
            steps {
                echo "Verify ${params.component} ${params.package} to environment ${params.env}"
                withCredentials([sshUserPrivateKey(credentialsId: 'AwsCentosPem', keyFileVariable: 'AWS_CENTOS_PEM')]) {
                    sh """
                        export ANSIBLE_CONFIG=ansible/ansible.cfg
                        export ANSIBLE_FORCE_COLOR=true
                        echo \"${env.DEPLOY_CRED}\" > .vault_password
                        ansible-playbook -i ansible/inventories/staging/${params.env}/hosts \
                            --vault-password-file .vault_password \
                            --private-key ${env.AWS_CENTOS_PEM} \
                            ansible/playbooks/maintenance/status_omnius.yml
                        rm -rf .vault_password
                    """
                }
            }
        }
    }

    post {
        failure {
            script {
                echo 'Failure!'

                if (buildingTag()) {
                    env.message = "Deployment failed: <${env.JOB_DISPLAY_URL}|${env.JOB_NAME}> <${env.RUN_DISPLAY_URL}|#${env.BUILD_NUMBER}>"

                    slackSend channel: env.channel, color: 'danger', message: env.message
                    if (env.slackChannel != '') {
                        slackSend channel: params.slackChannel, color: 'danger', message: env.message
                    }
                }
            }
        }

        success {
            script {
                echo 'Success!'

                if (buildingTag()) {
                    env.message = "Deployment succeeded: <${env.JOB_DISPLAY_URL}|${env.JOB_NAME}> <${env.RUN_DISPLAY_URL}|#${env.BUILD_NUMBER}>"

                    slackSend channel: env.channel, color: 'good', message: env.message
                    if (env.slackChannel != '') {
                        slackSend channel: params.slackChannel, color: 'good', message: env.message
                    }
                }
            }
        }
    }
}
