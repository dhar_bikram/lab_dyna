#!/bin/bash

function usage () {
  grep '^##' "$0" | sed -e 's/^##//' -e "s/__PROG__/$me/" 1>&2
}

function err () {
  echo  1>&2
  echo "$(tput setab 1) $1 $(tput sgr0)" 1>&2
}
