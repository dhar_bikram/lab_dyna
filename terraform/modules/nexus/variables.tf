## from other modules
variable "security_group_default" {}

variable "subnet_id" {}
variable "project" {}
variable "key_name" {}
variable "aws_region" {}
variable "zone_id" {}
variable "zone_name" {}
variable "s3-bucket" {}
variable create_s3 {}
variable "nexus_name" {}
variable "docker_name" {}

variable "nginx_name" {}

variable "nexus_volume_size" {}

variable "nexus_extra_vol" {}

variable "dns_record" {}
variable nexus_eip {}
variable ass_public_ip {}

variable "nexus-amis" {
  type        = "map"
  description = "AMIs by region"

  default = {
    ap-southeast-1 = "ami-16a4fe6a"
    eu-central-1   = "ami-337be65c"
  }
}

variable "nexus-ami-ena" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}
