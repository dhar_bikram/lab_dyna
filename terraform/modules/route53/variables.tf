#variables from main file
variable "project" {}

#-------------------------------------------------------------------------------
# If primary_zone is true, it will create a new Hosted Zone
# Be careful to enable, it will create other hosted zone with same name,
# if available alreaady
#-------------------------------------------------------------------------------
variable "primary_zone" {
  description = "Create primary zone, keep Boolean"
  default     = "false"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone, Make  primary_zone = false and add zone_id
#-------------------------------------------------------------------------------
variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}
