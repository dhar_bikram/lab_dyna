data "aws_instance" "kubernetes_master" {
  filter {
    name   = "tag:k8s.io/role/master"
    values = ["1"]
  }
}

resource "aws_route53_record" "k8s_private_record" {
  count   = "${length(var.k8s_private)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.k8s_private, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["af5044345336511e996fd0250d90b464-763250671.eu-central-1.elb.amazonaws.com"]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "api_kube_priv_record" {
  zone_id = "${var.zone_id_priv}"
  name    = "api.kube.dynacommercelab.com"
  type    = "A"
  ttl     = "300"
  records = ["${data.aws_instance.kubernetes_master.public_ip}"] // IP of Kube master
}
