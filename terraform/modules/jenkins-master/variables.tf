## from other modules
variable "security_group_wfh" {}

variable "security_group_default" {}
variable "subnet_id" {}
variable "project" {}
variable "key_name" {}
variable "aws_region" {}
variable "zone_id" {}
variable "zone_name" {}

variable "jenkins-ami-ena" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}
