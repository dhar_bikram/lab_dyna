variable "project" {}
variable "environment" {}

variable "dynamodb_table" {}

variable "table_name" {}
variable "hash_key" {}
variable "s3_bucket_backend" {}

variable "aws_account" {}

variable "iam_users" {
  type = "map"
}

variable "source_ip" {
  type = "map"
}

variable "create_s3" {}
