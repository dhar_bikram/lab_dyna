variable "aws_region" {}
variable "key_name" {}
variable "security_group_wfh" {}
variable "security_group_default" {}
variable "subnet_id" {}
variable "project" {}
variable "zone_id" {}
variable "zone_id_priv" {}
variable "zone_name" {}
variable "bitbucket_ipsv4_sg" {}

variable "bitbucket_ipsv6_sg" {}

variable "amis" {
  type        = "map"
  description = "AMIs by region"

  default = {
    ap-southeast-1 = "ami-16a4fe6a"
    eu-central-1   = "ami-337be65c"
  }
}
