resource "aws_instance" "nginx" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "t2.micro"

  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.security_group_wfh}", "${var.security_group_default}", "${var.bitbucket_ipsv4_sg}", "${var.bitbucket_ipsv6_sg}"]
  subnet_id                   = "${var.subnet_id}"
  associate_public_ip_address = "true"

  tags {
    Name = "${var.project}-nginx"
  }

  volume_tags {
    Name = "${var.project}-nginx-vol"
    Role = "volume"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_eip" "nginx-eip" {
  instance = "${aws_instance.nginx.id}"
  vpc      = true

  tags {
    Name = "${var.project}-nginxeip"
  }
}

resource "aws_route53_record" "nginx_record" {
  zone_id = "${var.zone_id}"
  name    = "nginx.${var.zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.nginx-eip.public_ip}"]
}

resource "aws_route53_record" "nginx_private_record" {
  zone_id = "${var.zone_id_priv}"
  name    = "nginx.${var.zone_name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.nginx.private_ip}"]
}
