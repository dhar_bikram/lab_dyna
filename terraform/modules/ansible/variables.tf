## from other modules
variable "security_group_default" {}

variable "subnet_id" {}

variable "project" {}

variable "key_name" {}
variable "aws_region" {}

variable "ansible-amis" {
  type        = "map"
  description = "AMIs by region"

  default = {
    ap-southeast-1 = "ami-16a4fe6a"
    eu-central-1   = "ami-337be65c"
  }
}
