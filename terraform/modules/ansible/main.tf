resource "aws_instance" "ansible" {
  ami                         = "${lookup(var.ansible-amis, var.aws_region)}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${var.security_group_default}"]
  subnet_id                   = "${var.subnet_id}"
  associate_public_ip_address = "false"

  tags {
    Environment = "${var.project}-ansible"
    Name        = "${var.project}-ansible"
    Project     = "${var.project}"
    Role        = "provisioning"
  }

  volume_tags {
    Environment = "${var.project}-ansible"
    Name        = "${var.project}-ansible-vol"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "40"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}
