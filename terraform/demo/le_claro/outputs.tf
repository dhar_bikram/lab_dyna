output "CLARO_WEB" {
  value = "${aws_instance.demo_webserver_claro.private_dns}"
}

output "LE_WEB" {
  value = "${aws_instance.demo_webserver_le.private_dns}"
}
