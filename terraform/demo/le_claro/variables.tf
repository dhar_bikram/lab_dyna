variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "project" {
  description = "Created by terraform"
  default     = "demo"
}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "demo"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone
#-------------------------------------------------------------------------------

variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}

variable "zone_id_priv" {
  description = "dynacommecelab.com ZONE ID"
  default     = "Z1EJMG1AOLMAH"
}

variable "ami_st_ena" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}

variable "claro_dns" {
  default = [
    "telesales-demo-claro.dynacommercelab.com",
    "retail-demo-claro.dynacommercelab.com",
    "indirect-demo-claro.dynacommercelab.com",
    "restapi-demo-claro.dynacommercelab.com",
    "rabbitmq-demo-claro.dynacommercelab.com",
  ]
}

variable "le_dns" {
  default = [
    "telesales-demo-le.dynacommercelab.com",
    "retail-demo-le.dynacommercelab.com",
    "indirect-demo-le.dynacommercelab.com",
    "restapi-demo-le.dynacommercelab.com",
    "rabbitmq-demo-le.dynacommercelab.com",
  ]
}
