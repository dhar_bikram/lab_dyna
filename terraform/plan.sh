#!/usr/bin/env bash
##
## USAGE: __PROG__
##
## This __PROG__ script can be used to execute the plan for a terraform
## script to check the impact before applying.
##
## Usage example:
##   ./__PROG__ production/continuous-integration
##
## Commands:
## - ./__PROG__ <terraform-plan>   shows you the plan for this environment
## - ./__PROG__ -h                 Shows this help message
## - ./__PROG__ --help             Shows this help message

case "$(uname -s)" in
    Linux*|MINGW64*)
        SCRIPTPATH=$(dirname "$(readlink -f "$0")")
        PROJECTROOT=$SCRIPTPATH/..
    ;;
    Darwin*)
        SCRIPTPATH=$(dirname "$(greadlink -f "$0")")
        PROJECTROOT=$SCRIPTPATH/..
    ;;
    *)
        echo "Could not determine script path"
        exit 1
        ;;
esac
me=`basename "$0"`
ns=$1

source $SCRIPTPATH/utils.sh

if [ -z "$1" ] ; then
  err "The folder to your environment is mandatory."
  usage
  exit 1
fi

ENVIRONMENT=$1

if [ ! -d "$SCRIPTPATH/$ENVIRONMENT" ] ; then
  err "The provided folder does not exist."
  usage
  exit 1
fi
pushd $SCRIPTPATH/$ENVIRONMENT > /dev/null

terraform plan

popd > /dev/null
