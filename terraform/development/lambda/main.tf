provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "lambda.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "events.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "iam_for_lambda_policy" {
  name = "iam_for_lambda_policy"
  role = "${aws_iam_role.iam_for_lambda.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
    ],
      "Resource": [
        "arn:aws:logs:*:*:*"
    ]
  },
    {
      "Effect": "Allow",
      "Action": [
      "ec2:StartInstances",
      "ec2:StopInstances"
    ],
      "Resource": "arn:aws:ec2:*:*:instance/*"
    },
    {
      "Effect": "Allow",
      "Action": "ec2:DescribeInstances",
      "Resource": "*"
    }
  ]
}
EOF
}

data "archive_file" "ec2_instances_start_zip" {
  type        = "zip"
  source_file = "${path.module}/../../../aws-lambda/ec2_instances_start.js"
  output_path = "${path.module}/ec2_instances_start.zip"
}

data "archive_file" "ec2_instances_stop_zip" {
  type        = "zip"
  source_file = "${path.module}/../../../aws-lambda/ec2_instances_stop.js"
  output_path = "${path.module}/ec2_instances_stop.zip"
}

# start lambda function
resource "aws_cloudwatch_event_rule" "start_ec2_event" {
  name                = "Start_EC2_Instances"
  description         = "Start EC2 on Weekdays"
  schedule_expression = "cron(0 3 ? * MON-FRI *)"
}

resource "aws_cloudwatch_event_target" "start_ec2_instances" {
  target_id = "ec2_instances_start"
  arn       = "${aws_lambda_function.ec2_instances_start.arn}"
  rule      = "${aws_cloudwatch_event_rule.start_ec2_event.name}"
}

resource "aws_lambda_function" "ec2_instances_start" {
  filename         = "ec2_instances_start.zip"
  function_name    = "ec2_instances_start"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  handler          = "ec2_instances_start.handler"
  runtime          = "nodejs8.10"
  source_code_hash = "${data.archive_file.ec2_instances_start_zip.output_base64sha256}"
  timeout          = 10
}

resource "aws_lambda_permission" "start_event_update" {
  statement_id  = "start_event_update"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ec2_instances_start.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.start_ec2_event.arn}"
}

# stop lambda function
resource "aws_cloudwatch_event_rule" "stop_ec2_event" {
  name                = "Stop_EC2_Instances"
  description         = "Stop EC2 on Weekdays"
  schedule_expression = "cron(0 18 ? * MON-FRI *)"
}

resource "aws_cloudwatch_event_target" "stop_ec2_instances" {
  target_id = "ec2_instances_stop"
  arn       = "${aws_lambda_function.ec2_instances_stop.arn}"
  rule      = "${aws_cloudwatch_event_rule.stop_ec2_event.name}"
}

resource "aws_lambda_function" "ec2_instances_stop" {
  filename         = "ec2_instances_stop.zip"
  function_name    = "ec2_instances_stop"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  handler          = "ec2_instances_stop.handler"
  runtime          = "nodejs8.10"
  source_code_hash = "${data.archive_file.ec2_instances_stop_zip.output_base64sha256}"
  timeout          = 10
}

resource "aws_lambda_permission" "stop_event_update" {
  statement_id  = "stop_event_update"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ec2_instances_stop.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.stop_ec2_event.arn}"
}

#### lambda function for schedule snapshots of sonarqube ebs volumes

data "archive_file" "ebs_create_snapshot_zip" {
  type        = "zip"
  source_file = "${path.module}/../../../aws-lambda/ebs_create_snapshot.js"
  output_path = "${path.module}/ebs_create_snapshot_lambda_function.zip"
}

resource "aws_lambda_function" "ebs_create_snapshot" {
  filename         = "ebs_create_snapshot_lambda_function.zip"
  function_name    = "ebs_create_snapshot"
  role             = "${aws_iam_role.ebs_snapshot_role.arn}"
  handler          = "ebs_create_snapshot.handler"
  source_code_hash = "${data.archive_file.ebs_create_snapshot_zip.output_base64sha256}"
  runtime          = "nodejs8.10"

  tags {
    Environment = "development-dclab"
    Name        = "ebs_create_snapshot"
    Project     = "dclab"
    Role        = "lambda"
  }
}

data "archive_file" "ebs_delete_snapshot_zip" {
  type        = "zip"
  source_file = "${path.module}/../../../aws-lambda/ebs_delete_snapshot.js"
  output_path = "${path.module}/ebs_delete_snapshot_lambda_function.zip"
}

resource "aws_lambda_function" "ebs_delete_snapshot" {
  filename         = "ebs_delete_snapshot_lambda_function.zip"
  function_name    = "ebs_delete_snapshot"
  role             = "${aws_iam_role.ebs_snapshot_role.arn}"
  handler          = "ebs_delete_snapshot.handler"
  source_code_hash = "${data.archive_file.ebs_delete_snapshot_zip.output_base64sha256}"
  runtime          = "nodejs8.10"

  tags {
    Environment = "development-dclab"
    Name        = "ebs_delete_snapshot"
    Project     = "dclab"
    Role        = "lambda"
  }
}

resource "aws_iam_role" "ebs_snapshot_role" {
  name = "ebs_snapshot_role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "lambda.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ebs_snapshot_policy" {
  name = "ebs_snapshot_policy"
  role = "${aws_iam_role.ebs_snapshot_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:CreateSnapshot",
                "ec2:DeleteSnapshot",
                "ec2:CreateTags",
                "ec2:ModifySnapshotAttribute",
                "ec2:ResetSnapshotAttribute"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_cloudwatch_event_rule" "ebs_create_snapshot" {
  name                = "ebs_create_snapshot"
  description         = "Fires every first day of the month"
  schedule_expression = "cron(0 12 1 * ? *)"
}

resource "aws_cloudwatch_event_target" "ebs_create_snapshot_every_month" {
  rule      = "${aws_cloudwatch_event_rule.ebs_create_snapshot.name}"
  target_id = "ebs_create_snapshot"
  arn       = "${aws_lambda_function.ebs_create_snapshot.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_ebs_create_snapshot" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ebs_create_snapshot.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.ebs_create_snapshot.arn}"
}

resource "aws_cloudwatch_event_rule" "ebs_delete_snapshot" {
  name                = "ebs_delete_snapshot"
  description         = "Fires every last day of the month"
  schedule_expression = "cron(0 12 L * ? *)"
}

resource "aws_cloudwatch_event_target" "ebs_delete_snapshot_every_month" {
  rule      = "${aws_cloudwatch_event_rule.ebs_delete_snapshot.name}"
  target_id = "ebs_delete_snapshot"
  arn       = "${aws_lambda_function.ebs_delete_snapshot.arn}"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_ebs_delete_snapshot" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.ebs_delete_snapshot.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.ebs_delete_snapshot.arn}"
}
