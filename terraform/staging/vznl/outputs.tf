output "VZNL_ST1_WEB" {
  value = "${aws_instance.vznl_webserver_st1.private_dns}"
}

output "VZNL_DB" {
  value = "${aws_instance.vznl_db_server.private_dns}"
}

output "VZNL_RABBITMQ" {
  value = "${aws_instance.vznl_rabbitmq_server.private_dns}"
}

output "VZNL_REDIS" {
  value = "${aws_instance.vznl_redis_server.private_dns}"
}

output "VZNL_ST1_OIL_AIO" {
  value = "${aws_instance.vznl_windowsserver_st1_aio.private_dns}"
}

output "VZNL_ST2_WEB" {
  value = "${aws_instance.vznl_webserver_st2.private_dns}"
}

output "VZNL_ST2_OIL_AIO" {
  value = "${aws_instance.vznl_windowsserver_st2_aio.private_dns}"
}

output "VZNL_ST3_WEB" {
  value = "${aws_instance.vznl_webserver_st3.private_dns}"
}

output "VZNL_ST3_OIL_AIO" {
  value = "${aws_instance.vznl_windowsserver_st3_aio.private_dns}"
}

output "VZNL_ST4_WEB" {
  value = "${aws_instance.vznl_webserver_st4.private_dns}"
}

output "VZNL_ST4_OIL_APP1" {
  value = "${aws_instance.vznl_windowsserver_st4_app1.private_dns}"
}

output "VZNL_ST4_OIL_APP2" {
  value = "${aws_instance.vznl_windowsserver_st4_app2.private_dns}"
}

output "VZNL_ST4_OIL_MSSQL" {
  value = "${aws_instance.vznl_windowsserver_st4_mssql.private_dns}"
}

output "VZNL_ST5_WEB" {
  value = "${aws_instance.vznl_webserver_st5.private_dns}"
}

output "VZNL_ST5_OIL_APP1" {
 value = "${aws_instance.vznl_windowsserver_st5_aio.private_dns}"
}

output "VZNL_ST6_WEB" {
  value = "${aws_instance.vznl_webserver_st6.private_dns}"
}

output "VZNL_ST6_OIL_AIO" {
  value = "${aws_instance.vznl_windowsserver_st6_aio.private_dns}"
}

output "VZNL_ST7_WEB" {
  value = "${aws_instance.vznl_webserver_st7.private_dns}"
}

output "VZNL_ST7_OIL_AIO" {
  value = "${aws_instance.vznl_windowsserver_st7_aio.private_dns}"
}
