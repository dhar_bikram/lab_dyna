provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "staging-vznl.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

data "template_file" "win_user_data" {
  template = "${file("../../templates/userdata.ps1")}"
}

resource "aws_instance" "vznl_db_server" {
  ami                    = "${lookup(var.ami_st, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-database"
    Environment = "${var.project}-st1,${var.project}-st2,${var.project}-st3,${var.project}-st4,${var.project}-st5"
    Project     = "${var.project}"
    Role        = "database"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "vznl_rabbitmq_server" {
  ami                    = "${lookup(var.ami_st, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-rabbitmq"
    Environment = "${var.project}-st1,${var.project}-st2,${var.project}-st3,${var.project}-st4,${var.project}-st5"
    Project     = "${var.project}"
    Role        = "rabbitmq"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "vznl_redis_server" {
  ami                    = "${lookup(var.ami_st, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-redis"
    Environment = "${var.project}-st1,${var.project}-st2,${var.project}-st3,${var.project}-st4,${var.project}-st5"
    Project     = "${var.project}"
    Role        = "redis"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### ST1 Terraform Resources #####
resource "aws_instance" "vznl_webserver_st1" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  volume_tags {
    Name        = "${var.project}-st1-web-vol"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st1-web"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "webserver"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st1_dns_record" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st1_dns_record_priv" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_instance" "vznl_windowsserver_st1_aio" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t2.xlarge"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "100"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  volume_tags {
    Name        = "${var.project}-st1-biztalk-aio-vol"
    Environment = "${var.project}-st1-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st1-biztalk-aio"
    Environment = "${var.project}-st1-biztalk"
    Project     = "${var.project}"
    Role        = "webserver,database"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### ST2 terraform Resources #####

resource "aws_instance" "vznl_webserver_st2" {
  ami                    = "${lookup(var.ami_st, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-st2-web"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st2_dns_record" {
  count   = "${length(var.st2_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st2_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st2_dns_record_priv" {
  count   = "${length(var.st2_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st2_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_instance" "vznl_windowsserver_st2_aio" {
  ami                    = "ami-08c8363a532babf1b"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st2-biztalk-aio-vol"
    Environment = "${var.project}-st2-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st2-biztalk-aio"
    Environment = "${var.project}-st2-biztalk"
    Project     = "${var.project}"
    Role        = "webserver,database"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st2_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st2-biztalk-aio-vol2"
    Environment = "${var.project}-st2-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st2_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st2_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st2_aio.id}"
}

##### ST3 terraform Resources #####

resource "aws_instance" "vznl_webserver_st3" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st3-web-vol"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st3-web"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "webserver"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st3_dns_record" {
  count   = "${length(var.st3_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st3_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st3_dns_record_priv" {
  count   = "${length(var.st3_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st3_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

### ST3 Windows with domain configuration

resource "aws_instance" "vznl_windowsserver_st3_aio" {
  ami                    = "ami-0d55859ee09a59138"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st3-biztalk-aio-vol"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st3-biztalk-aio-vol"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "webserver,database"
    hostname    = "vznlst3aio"                         // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st3_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st3-biztalk-aio-vol2"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st3_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st3_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st3_aio.id}"
}

resource "aws_ssm_association" "add_vznl_st3_aio_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st3_aio.id}"
}

##### ST4 terraform environment #####

### ST4 OSE Webserver
resource "aws_instance" "vznl_webserver_st4" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st4-web-vol"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st4-web"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st4_dns_record" {
  count   = "${length(var.st4_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st4_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st4_dns_record_priv" {
  count   = "${length(var.st4_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st4_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

### ST4 Windows APP1
resource "aws_instance" "vznl_windowsserver_st4_app1" {
  ami                    = "ami-0d55859ee09a59138"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st4-biztalk-app1-vol1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st4-biztalk-app1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "webserver"
    hostname    = "vznlst4app1"                     // only for windows this host name field added
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st4_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st4-biztalk-app1-vol2"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st4_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st4_app1_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st4_app1.id}"
}

resource "aws_ssm_association" "add_vznl_st4_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st4_app1.id}"
}

### ST4 Windows APP2
resource "aws_instance" "vznl_windowsserver_st4_app2" {
  ami                    = "ami-0d55859ee09a59138"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st4-biztalk-app2-vol1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st4-biztalk-app2-vol1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "webserver"
    hostname    = "vznlst4app2"                          // only for windows this host name field added
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st4_app2_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st4-biztalk-app2-vol2"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st4_app2_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st4_app2_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st4_app2.id}"
}

resource "aws_ssm_association" "add_vznl_st4_app2_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st4_app2.id}"
}

### ST4 Windows MSSQL
resource "aws_instance" "vznl_windowsserver_st4_mssql" {
  ami                    = "ami-0d55859ee09a59138"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st4-biztalk-mssql-vol1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st4-biztalk-mssql"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vznlst4mssql"                     // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st4_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st4-biztalk-mssql-vol2"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st4_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st4_mssql_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st4_mssql.id}"
}

resource "aws_ssm_association" "add_vznl_st4_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st4_mssql.id}"
}

### Reporting Server ###

resource "aws_instance" "vznl_windows_reportingserver" {
  ami                    = "ami-0b3cf9938f82f4ea1"                                           // Reporting server AMI as per request in OVI-462
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-reporting-vol1"
    Environment = "${var.project}-st1,${var.project}-st2,${var.project}-st3,${var.project}-st4,${var.project}-st5"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-reporting"
    Environment = "${var.project}-st1,${var.project}-st2,${var.project}-st3,${var.project}-st4,${var.project}-st5"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vznlreportingmssql"                                                                             // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windows_reportingserver_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-reporting-vol2"
    Environment = "${var.project}-st1,${var.project}-st2,${var.project}-st3,${var.project}-st4,${var.project}-st5"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windows_reportingserver_st4_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windows_reportingserver_mssql_drive_d.id}"
  instance_id = "${aws_instance.vznl_windows_reportingserver.id}"
}

resource "aws_ssm_association" "add_vznl_reportingserver_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windows_reportingserver.id}"
}

### ST5 OSE Webserver
resource "aws_instance" "vznl_webserver_st5" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st5-web-vol"
    Environment = "staging"
    Project     = "${var.project}"
    Role        = "webserver"
  }

  tags {
    Name        = "${var.project}-st5-web"
    Environment = "staging"
    Project     = "${var.project}"
    Role        = "Webserver"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st5_dns_record" {
  count   = "${length(var.st5_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st5_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st5_dns_record_priv" {
  count   = "${length(var.st5_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st5_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

### ST5 Windows APP1
resource "aws_instance" "vznl_windowsserver_st5_aio" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"
  credit_specification {
    cpu_credits = "unlimited"
  }
  volume_tags {
    Name        = "${var.project}-st5-biztalk-aio-vol"
    Environment = "${var.project}-st5-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st5-biztalk-aio"
    Environment = "${var.project}-st5-biztalk"
    Project     = "${var.project}"
    Role        = "webserver,database"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
    hostname    = "vznlst5aio"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st5_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st5-biztalk-aio-vol2"
    Environment = "${var.project}-st5-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st5_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st5_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st5_aio.id}"
}

resource "aws_ssm_association" "add_vznl_st5_aio_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st5_aio.id}"
}

##### ST6 terraform Resources #####

resource "aws_instance" "vznl_webserver_st6" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-st6-web"
    Environment = "${var.project}-st6"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st6_dns_record" {
  count   = "${length(var.st6_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st6_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st6_dns_record_priv" {
  count   = "${length(var.st6_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st6_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_instance" "vznl_windowsserver_st6_aio" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st6-biztalk-aio-vol"
    Environment = "${var.project}-st6-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st6-biztalk-aio"
    Environment = "${var.project}-st6-biztalk"
    Project     = "${var.project}"
    Role        = "webserver,database"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
    hostname    = "vznlst6aio"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st6_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st6-biztalk-aio-vol2"
    Environment = "${var.project}-st6-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st6_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st6_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st6_aio.id}"
}

resource "aws_ssm_association" "add_vznl_st6_aio_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st6_aio.id}"
}

##### ST7 terraform Resources #####

resource "aws_instance" "vznl_webserver_st7" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st7-web"
    Environment = "${var.project}-st7"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st7_dns_record" {
  count   = "${length(var.st7_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st7_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st7_dns_record_priv" {
  count   = "${length(var.st7_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st7_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_instance" "vznl_windowsserver_st7_aio" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.xlarge"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-st7-biztalk-aio-vol"
    Environment = "${var.project}-st7-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st7-biztalk-aio"
    Environment = "${var.project}-st7-biztalk"
    Project     = "${var.project}"
    Role        = "webserver,database"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
    hostname    = "vznlst7aio"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vznl_windowsserver_st7_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st7-biztalk-aio-vol2"
    Environment = "${var.project}-st7-biztalk"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vznl_windowsserver_st7_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vznl_windowsserver_st7_drive_d.id}"
  instance_id = "${aws_instance.vznl_windowsserver_st7_aio.id}"
}

resource "aws_ssm_association" "add_vznl_st7_aio_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vznl_windowsserver_st7_aio.id}"
}
