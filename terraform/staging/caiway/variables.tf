variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "project" {
  description = "Created by terraform"
  default     = "staging-caiway"
}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone
#-------------------------------------------------------------------------------

variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}

variable "zone_id_priv" {
  description = "dynacommecelab.com ZONE ID"
  default     = "Z1EJMG1AOLMAH"
}

variable "caiway_amis" {
  type        = "map"
  description = "AMIs by region"

  default = {
    ap-southeast-1 = "ami-16a4fe6a"
    eu-central-1   = "ami-337be65c"
  }
}

variable "st1_dns" {
  default = [
    "st1-shop-caiway.dynacommercelab.com",
    "st1-webshop-caiway.dynacommercelab.com",
  ]
}
