provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "staging-caiway.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

resource "aws_instance" "caiway_aio" {
  ami                    = "${lookup(var.caiway_amis, var.aws_region)}"
  instance_type          = "t2.medium"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  ebs_block_device {
    device_name           = "/dev/sdb"
    volume_size           = "30"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags {
    Name        = "${var.project}-st1-aio"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "webserver,database"
  }

  volume_tags {
    Name        = "${var.project}-st1-vol"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }

  root_block_device {
    volume_size           = "20"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "caiway_st1_record" {
  zone_id = "${var.zone_id}"
  name    = "st1-shop-caiway.${var.zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "caiway_st1_webshop_record" {
  zone_id = "${var.zone_id}"
  name    = "st1-webshop-caiway.${var.zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st1_dns_record_priv" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}
