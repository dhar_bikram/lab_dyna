provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "staging-vfde.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

data "template_file" "win_user_data" {
  template = "${file("../../templates/userdata.ps1")}"
}

resource "aws_instance" "vfde_db_server" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-database"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "database"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "vfde_rabbitmq_server" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-rabbitmq"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "rabbitmq"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "vfde_redis_server" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-redis-database"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "redis"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### ST1 VFDE OSE
resource "aws_instance" "vfde_webserver_st1" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st1-web"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st1_dns_record" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st1_dns_record_priv" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### ST1 OIL env

resource "aws_instance" "vfde_windowsserver_st1_app1" {
  ami                    = "ami-0d55859ee09a59138"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st1-windows-app-vol1"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st1-windows-app"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest1app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st1_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st1-app1-vol2"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st1_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st1_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st1_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st1_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st1_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st1_mssql" {
  ami                    = "ami-0d55859ee09a59138"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-st1-mssql-vol1"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st1-mssql"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest1dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st1_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st1-mssql-vol2"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st1_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st1_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st1_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st1_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st1_mssql.id}"
}

##### ST2 VFDE OSE
resource "aws_instance" "vfde_webserver_st2" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st2-web"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st2_dns_record" {
  count   = "${length(var.st2_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st2_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st2_dns_record_priv" {
  count   = "${length(var.st2_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st2_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### ST2 OIL env

resource "aws_instance" "vfde_windowsserver_st2_app1" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st2-windows-app-vol1"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st2-windows-app"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest2app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st2_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st2-app1-vol2"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st2_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st2_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st2_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st2_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st2_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st2_mssql" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st2-mssql-vol1"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st2-mssql"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest2dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st2_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st2-mssql-vol2"
    Environment = "${var.project}-st2"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st2_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st2_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st2_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st2_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st2_mssql.id}"
}

##### st3 VFDE OSE
resource "aws_instance" "vfde_webserver_st3" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st3-web"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st3_dns_record" {
  count   = "${length(var.st3_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st3_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st3_dns_record_priv" {
  count   = "${length(var.st3_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st3_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### st3 OIL env

resource "aws_instance" "vfde_windowsserver_st3_app1" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st3-windows-app-vol1"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st3-windows-app"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest3app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st3_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st3-app1-vol2"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st3_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st3_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st3_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st3_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st3_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st3_mssql" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st3-mssql-vol1"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st3-mssql"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest3dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st3_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st3-mssql-vol2"
    Environment = "${var.project}-st3"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st3_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st3_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st3_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st3_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st3_mssql.id}"
}

##### ST4 VFDE OSE
resource "aws_instance" "vfde_webserver_st4" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st4-web"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st4_dns_record" {
  count   = "${length(var.st4_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st4_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st4_dns_record_priv" {
  count   = "${length(var.st4_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st4_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### ST4 OIL env

resource "aws_instance" "vfde_windowsserver_st4_app1" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st4-windows-app-vol1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st4-windows-app"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest4app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st4_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st4-app1-vol2"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st4_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st4_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st4_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st4_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st4_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st4_mssql" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st4-mssql-vol1"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st4-mssql"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest4dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st4_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st4-mssql-vol2"
    Environment = "${var.project}-st4"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st4_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st4_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st4_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st4_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st4_mssql.id}"
}

##### st5 VFDE OSE
resource "aws_instance" "vfde_webserver_st5" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st5-web"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st5_dns_record" {
  count   = "${length(var.st5_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st5_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st5_dns_record_priv" {
  count   = "${length(var.st5_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st5_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### ST5 OIL env

resource "aws_instance" "vfde_windowsserver_st5_app1" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st5-windows-app-vol1"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st5-windows-app"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest5app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st5_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st5-app1-vol2"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st5_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st5_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st5_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st5_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st5_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st5_mssql" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st5-mssql-vol1"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st5-mssql"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest5dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st5_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st5-mssql-vol2"
    Environment = "${var.project}-st5"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st5_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st5_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st5_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st5_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st5_mssql.id}"
}

##### ST6 VFDE OSE
resource "aws_instance" "vfde_webserver_st6" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st6-web"
    Environment = "${var.project}-st6"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st6_dns_record" {
  count   = "${length(var.st6_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st6_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st6_dns_record_priv" {
  count   = "${length(var.st6_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st6_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

##### ST7 VFDE OSE
resource "aws_instance" "vfde_webserver_st7" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st7-web"
    Environment = "${var.project}-st7"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st7_dns_record" {
  count   = "${length(var.st7_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st7_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st7_dns_record_priv" {
  count   = "${length(var.st7_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st7_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

##### ST8 VFDE OSE
resource "aws_instance" "vfde_webserver_st8" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st8-web"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st8_dns_record" {
  count   = "${length(var.st8_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st8_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st8_dns_record_priv" {
  count   = "${length(var.st8_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st8_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### ST8 OIL env

resource "aws_instance" "vfde_windowsserver_st8_app1" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st8-windows-app-vol1"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st8-windows-app"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest8app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st8_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st8-app1-vol2"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st8_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st8_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st8_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st8_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st8_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st8_mssql" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st8-mssql-vol1"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st8-mssql"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest8dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st8_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st8-mssql-vol2"
    Environment = "${var.project}-st8"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st8_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st8_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st8_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st8_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st8_mssql.id}"
}

##### ST9 VFDE OSE
resource "aws_instance" "vfde_webserver_st9" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st9-web"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st9_dns_record" {
  count   = "${length(var.st9_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st9_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st9_dns_record_priv" {
  count   = "${length(var.st9_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st9_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

#### st9 OIL env

resource "aws_instance" "vfde_windowsserver_st9_app1" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st9-windows-app-vol1"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st9-windows-app"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "biztalk"
    hostname    = "vfdest9app1"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st9_app1_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-windows-st9-app1-vol2"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windowsserver_st9_app1_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st9_app1_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st9_app1.id}"
}

resource "aws_ssm_association" "add_vfde_st9_app1_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st9_app1.id}"
}

resource "aws_instance" "vfde_windowsserver_st9_mssql" {
  ami                    = "ami-5041293f"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  instance_type          = "t3.large"
  key_name               = "${var.aws_key_name}"
  iam_instance_profile   = "ec2-ssm-role-profile"
  user_data              = "${data.template_file.win_user_data.rendered}"

  credit_specification {
    cpu_credits = "unlimited"
  }

  volume_tags {
    Name        = "${var.project}-st9-mssql-vol1"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "volume"
  }

  tags {
    Name        = "${var.project}-st9-mssql"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "database"
    hostname    = "vfdest9dbserver"          // only for windows this host name field added
  }

  root_block_device {
    volume_size           = "50"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_ebs_volume" "vfde_windowsserver_st9_mssql_drive_d" {
  availability_zone = "${var.aws_region}a"
  size              = 100
  type              = "gp2"

  tags {
    Name        = "${var.project}-st9-mssql-vol2"
    Environment = "${var.project}-st9"
    Project     = "${var.project}"
    Role        = "volume"
  }
}

resource "aws_volume_attachment" "vfde_windwsserver_st9_mssql_drive_d_attach" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.vfde_windowsserver_st9_mssql_drive_d.id}"
  instance_id = "${aws_instance.vfde_windowsserver_st9_mssql.id}"
}

resource "aws_ssm_association" "add_vfde_st9_mssql_to_domain" {
  name        = "${data.terraform_remote_state.network.ssm_document_domain_name}"
  instance_id = "${aws_instance.vfde_windowsserver_st9_mssql.id}"
}

##### ST10 VFDE OSE
resource "aws_instance" "vfde_webserver_st10" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st10-web"
    Environment = "${var.project}-st10"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st10_dns_record" {
  count   = "${length(var.st10_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st10_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st10_dns_record_priv" {
  count   = "${length(var.st10_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st10_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

##### ST11 VFDE OSE
resource "aws_instance" "vfde_webserver_st11" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  tags {
    Name        = "${var.project}-st11-web"
    Environment = "${var.project}-st11"
    Project     = "${var.project}"
    Role        = "webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st11_dns_record" {
  count   = "${length(var.st11_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st11_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st11_dns_record_priv" {
  count   = "${length(var.st11_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st11_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}