output "VZDE_DATABASE" {
  value = "${aws_instance.vfde_db_server.private_dns}"
}

output "VZDE_RABBITMQ" {
  value = "${aws_instance.vfde_rabbitmq_server.private_dns}"
}

output "VFDE_REDIS" {
  value = "${aws_instance.vfde_redis_server.private_dns}"
}

## ST1 Output
output "VFDE_ST1_WEB" {
  value = "${aws_instance.vfde_webserver_st1.private_dns}"
}

output "VFDE_APP1_ST1" {
  value = "${aws_instance.vfde_windowsserver_st1_app1.private_dns}"
}

output "VFDE_MSSQL_ST1" {
  value = "${aws_instance.vfde_windowsserver_st1_mssql.private_dns}"
}

## ST2 Output
output "VFDE_ST2_WEB" {
  value = "${aws_instance.vfde_webserver_st2.private_dns}"
}

output "VFDE_APP1_ST2" {
  value = "${aws_instance.vfde_windowsserver_st2_app1.private_dns}"
}

output "VFDE_MSSQL_ST2" {
  value = "${aws_instance.vfde_windowsserver_st2_mssql.private_dns}"
}

## ST3 Output
output "VFDE_ST3_WEB" {
  value = "${aws_instance.vfde_webserver_st3.private_dns}"
}

output "VFDE_APP1_ST3" {
  value = "${aws_instance.vfde_windowsserver_st3_app1.private_dns}"
}

output "VFDE_MSSQL_ST3" {
  value = "${aws_instance.vfde_windowsserver_st3_mssql.private_dns}"
}

## ST4 Output
output "VFDE_ST4_WEB" {
  value = "${aws_instance.vfde_webserver_st4.private_dns}"
}

output "VFDE_APP1_ST4" {
  value = "${aws_instance.vfde_windowsserver_st4_app1.private_dns}"
}

output "VFDE_MSSQL_ST4" {
  value = "${aws_instance.vfde_windowsserver_st4_mssql.private_dns}"
}

## ST5 Output
output "VFDE_ST5_WEB" {
  value = "${aws_instance.vfde_webserver_st5.private_dns}"
}

output "VFDE_APP1_ST5" {
  value = "${aws_instance.vfde_windowsserver_st5_app1.private_dns}"
}

output "VFDE_MSSQL_ST5" {
  value = "${aws_instance.vfde_windowsserver_st5_mssql.private_dns}"
}

## ST6 Output
output "VFDE_ST6_WEB" {
  value = "${aws_instance.vfde_webserver_st6.private_dns}"
}

## ST7 Output
output "VFDE_ST7_WEB" {
  value = "${aws_instance.vfde_webserver_st7.private_dns}"
}

## ST8 Output
output "VFDE_ST8_WEB" {
  value = "${aws_instance.vfde_webserver_st8.private_dns}"
}

output "VFDE_APP1_ST8" {
  value = "${aws_instance.vfde_windowsserver_st8_app1.private_dns}"
}

output "VFDE_MSSQL_ST8" {
  value = "${aws_instance.vfde_windowsserver_st8_mssql.private_dns}"
}

## ST9 Output
output "VFDE_ST9_WEB" {
  value = "${aws_instance.vfde_webserver_st9.private_dns}"
}

output "VFDE_APP1_ST9" {
  value = "${aws_instance.vfde_windowsserver_st9_app1.private_dns}"
}

output "VFDE_MSSQL_ST9" {
  value = "${aws_instance.vfde_windowsserver_st9_mssql.private_dns}"
}

## ST10 Output
output "VFDE_ST10_WEB" {
  value = "${aws_instance.vfde_webserver_st10.private_dns}"
}

## ST11 Output
output "VFDE_ST11_WEB" {
  value = "${aws_instance.vfde_webserver_st11.private_dns}"
}