variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "project" {
  description = "Created by terraform"
  default     = "staging-vfde"
}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "staging"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone
#-------------------------------------------------------------------------------

variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}

variable "zone_id_priv" {
  description = "dynacommecelab.com ZONE ID"
  default     = "Z1EJMG1AOLMAH"
}

#-------------------------------------------------------------------------------
# Centos AMIS
#-------------------------------------------------------------------------------

variable "ami_st_ena" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}

variable "st1_dns" {
  default = [
    "st1-telesales-vfde.dynacommercelab.com",
    "st1-rabbitmq-vfde.dynacommercelab.com",
    "st1-oil-vfde.dynacommercelab.com",
    "st1-retail-vfde.dynacommercelab.com",
  ]
}

variable "st2_dns" {
  default = [
    "st2-telesales-vfde.dynacommercelab.com",
    "st2-rabbitmq-vfde.dynacommercelab.com",
    "st2-oil-vfde.dynacommercelab.com",
    "st2-retail-vfde.dynacommercelab.com",
  ]
}

variable "st3_dns" {
  default = [
    "st3-telesales-vfde.dynacommercelab.com",
    "st3-rabbitmq-vfde.dynacommercelab.com",
    "st3-oil-vfde.dynacommercelab.com",
    "st3-retail-vfde.dynacommercelab.com",
  ]
}

variable "st4_dns" {
  default = [
    "st4-telesales-vfde.dynacommercelab.com",
    "st4-rabbitmq-vfde.dynacommercelab.com",
    "st4-oil-vfde.dynacommercelab.com",
    "st4-retail-vfde.dynacommercelab.com",
  ]
}

variable "st5_dns" {
  default = [
    "st5-telesales-vfde.dynacommercelab.com",
    "st5-rabbitmq-vfde.dynacommercelab.com",
    "st5-oil-vfde.dynacommercelab.com",
    "st5-retail-vfde.dynacommercelab.com",
  ]
}

variable "st6_dns" {
  default = [
    "st6-telesales-vfde.dynacommercelab.com",
    "st6-rabbitmq-vfde.dynacommercelab.com",
  ]
}

variable "st7_dns" {
  default = [
    "st7-telesales-vfde.dynacommercelab.com",
    "st7-rabbitmq-vfde.dynacommercelab.com",
  ]
}

variable "st8_dns" {
  default = [
    "st8-telesales-vfde.dynacommercelab.com",
    "st8-rabbitmq-vfde.dynacommercelab.com",
    "st8-oil-vfde.dynacommercelab.com",
    "st8-retail-vfde.dynacommercelab.com",
  ]
}

variable "st9_dns" {
  default = [
    "st9-telesales-vfde.dynacommercelab.com",
    "st9-rabbitmq-vfde.dynacommercelab.com",
    "st9-oil-vfde.dynacommercelab.com",
    "st9-retail-vfde.dynacommercelab.com",
  ]
}

variable "st10_dns" {
  default = [
    "st10-telesales-vfde.dynacommercelab.com",
    "st10-rabbitmq-vfde.dynacommercelab.com",
    "st10-retail-vfde.dynacommercelab.com",
  ]
}

variable "st11_dns" {
  default = [
    "st11-telesales-vfde.dynacommercelab.com",
    "st11-rabbitmq-vfde.dynacommercelab.com",
    "st11-retail-vfde.dynacommercelab.com",
  ]
}