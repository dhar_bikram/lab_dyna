output "DELTA_DB" {
  value = "${aws_instance.delta_db_server.private_dns}"
}

output "DELTA_RABBITMQ" {
  value = "${aws_instance.delta_rabbitmq_server.private_dns}"
}

output "DELTA_REDIS" {
  value = "${aws_instance.delta_redis_server.private_dns}"
}

output "DELTA_ST1_PHP_WEB" {
  value = "${aws_instance.delta_webserver_st1-php.private_dns}"
}

output "DELTA_ST1_JAVA_WEB" {
  value = "${aws_instance.delta_webserver_st1-java.private_dns}"
}
