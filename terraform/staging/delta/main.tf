provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "staging-delta.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

##### Mysql Database  #####

resource "aws_instance" "delta_db_server" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-database-vol"
    Environment = "${var.project}-st1,${var.project}-st2"
    Project     = "${var.project}"
    Role        = "dbserver"
  }

  tags {
    Name        = "${var.project}-database"
    Environment = "${var.project}-st1,${var.project}-st2"
    Project     = "${var.project}"
    Role        = "dbserver"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### RabbitMQ  #####
resource "aws_instance" "delta_rabbitmq_server" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-rabbitmq-vol"
    Environment = "${var.project}-st1,${var.project}-st2"
    Project     = "${var.project}"
    Role        = "rabbitmq"
  }

  tags {
    Name        = "${var.project}-rabbitmq"
    Environment = "${var.project}-st1,${var.project}-st2"
    Project     = "${var.project}"
    Role        = "rabbitmq"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### Redis Database  #####
resource "aws_instance" "delta_redis_server" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-redis-vol"
    Environment = "${var.project}-st1,${var.project}-st2"
    Project     = "${var.project}"
    Role        = "redis"
  }

  tags {
    Name        = "${var.project}-redis-database"
    Environment = "${var.project}-st1,${var.project}-st2"
    Project     = "${var.project}"
    Role        = "redis"
  }

  root_block_device {
    volume_size           = "60"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

##### ST1 Delta terraform Resources #####
resource "aws_instance" "delta_webserver_st1-php" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-st1-php-vol"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "Webserver"
  }

  tags {
    Name        = "${var.project}-st1-php"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "Webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_instance" "delta_webserver_st1-java" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.large"
  vpc_security_group_ids = ["${data.terraform_remote_state.network.security_group_default}"]
  subnet_id              = "${data.terraform_remote_state.network.subnet_private_id}"
  key_name               = "${var.aws_key_name}"

  volume_tags {
    Name        = "${var.project}-st1-java-vol"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "Webserver"
  }

  tags {
    Name        = "${var.project}-st1-java"
    Environment = "${var.project}-st1"
    Project     = "${var.project}"
    Role        = "Webserver"
    Start       = "StartWeekDays"
    Stop        = "StopWeekDays"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "st1_dns_record" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}

resource "aws_route53_record" "st1_dns_record_priv" {
  count   = "${length(var.st1_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st1_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["nginx.${var.zone_name}"]
}
