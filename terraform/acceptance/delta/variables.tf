variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "project" {
  description = "Created by terraform"
  default     = "acc-delta"
}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "zone_name" {
  description = "DNS name for Zone"
  default     = "dynacommercelab.com"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "acceptance"
}

#-------------------------------------------------------------------------------
# Zone ID to create existing zone
#-------------------------------------------------------------------------------

variable "zone_id" {
  description = "dynacommecelab.com ZONE ID"
  default     = "ZJKKGPVBFJK09"
}

variable "zone_id_priv" {
  description = "dynacommecelab.com ZONE ID"
  default     = "Z1EJMG1AOLMAH"
}

variable "delta_priv_subnet_cidr" {
  description = "CIDR for the Private Subnet"
  default     = "20.0.3.0/24"                 ### Not a private IP
}

variable "ami_acc_delta" {
  type        = "map"
  description = "AMIs by region"

  default = {
    eu-central-1 = "ami-dd3c0f36"
  }
}

variable "acc2_dns" {
  default = [
    "uat2-telesales-delta.dynacommercelab.com",
    "uat2-rabbitmq-delta.dynacommercelab.com",
  ]
}

variable "vpn_connection_static_routes_destinations" {
  type    = "list"
  default = ["10.52.0.0/23", "192.168.10.0/23", "192.168.122.0/23", "192.168.8.0/24"] ## Delta Office IP Connections
}
