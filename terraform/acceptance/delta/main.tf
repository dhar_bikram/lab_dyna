provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "Acc-Delta.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}
data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "dyna-terraform-state"
    key    = "network.tfstate"
    region = "eu-central-1"
  }
}

resource "aws_subnet" "delta_private_subnet" {
  vpc_id = "${data.terraform_remote_state.network.dclab_vpc_id}"

  cidr_block        = "${var.delta_priv_subnet_cidr}"
  availability_zone = "${var.aws_region}a"

  tags {
    Name    = "${var.project}-private-Subnet"
    Project = "${var.project}"
  }
}

resource "aws_route_table" "delta_private_route" {
  vpc_id = "${data.terraform_remote_state.network.dclab_vpc_id}"

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = "${data.terraform_remote_state.network.nat_instance_id}"
  }
  tags = {
    Name = "${var.project}-private-route"
    Project = "${var.project}"
  }
}
resource "aws_route" "delta_cgw_route" {
    count = "${length(var.vpn_connection_static_routes_destinations)}"
    route_table_id = "${aws_route_table.delta_private_route.id}"
    destination_cidr_block = "${element(var.vpn_connection_static_routes_destinations, count.index)}"
    gateway_id = "${aws_customer_gateway.customer_gateway.id}"
  }
resource "aws_route_table_association" "route_delta_private_associate" {
  subnet_id      = "${aws_subnet.delta_private_subnet.id}"
  route_table_id = "${aws_route_table.delta_private_route.id}"
}

resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = "${data.terraform_remote_state.network.dclab_vpc_id}"
  tags = {
    Name = "${var.project}-vpn-gw"
    Project = "${var.project}"
  }
}

resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = 65000
  ip_address = "62.45.52.70"  ## Cusotmer IP
  type       = "ipsec.1"
  tags = {
    Name = "${var.project}-cgw"
    Project = "${var.project}"
  }
}

resource "aws_vpn_connection" "main" {
  vpn_gateway_id      = "${aws_vpn_gateway.vpn_gateway.id}"
  customer_gateway_id = "${aws_customer_gateway.customer_gateway.id}"
  type                = "ipsec.1"
  static_routes_only  = true
  tags = {
    Name = "${var.project}-vpn-connection"
    Project = "${var.project}"
  }
}

resource "aws_vpn_connection_route" "delta_office" {
  count = "${length(var.vpn_connection_static_routes_destinations)}"
  destination_cidr_block = "${element(var.vpn_connection_static_routes_destinations, count.index)}"
  vpn_connection_id      = "${aws_vpn_connection.main.id}"
}


##### UAT/ACC Delta terraform Resources for EC2 #####

resource "aws_instance" "delta_webserver_acc2" {
  ami                    = "${lookup(var.ami_st_ena, var.aws_region)}"
  instance_type          = "t3.xlarge"
  vpc_security_group_ids = [""] ### Will create a SG group
  subnet_id              = "${aws_subnet.delta_private_subnet.id}"
  key_name               = "${var.aws_key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  volume_tags {
    Name        = "${var.project}-acc2-web-vol"
    Environment = "acceptance"
    Project     = "${var.project}"
    Role        = "webserver"
  }

  tags {
    Name        = "${var.project}-acc2-web"
    Environment = "acceptance"
    Project     = "${var.project}"
    Role        = "Webserver"
  }

  root_block_device {
    volume_size           = "30"
    delete_on_termination = true
    volume_type           = "gp2"
  }
}

resource "aws_route53_record" "acc2_dns_record" {
  count   = "${length(var.acc2_dns)}"
  zone_id = "${var.zone_id}"
  name    = "${element(var.acc2_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["accnginx.${var.zone_name}"]
}

resource "aws_route53_record" "st3_dns_record_priv" {
  count   = "${length(var.st3_dns)}"
  zone_id = "${var.zone_id_priv}"
  name    = "${element(var.st3_dns, count.index)}"
  type    = "CNAME"
  ttl     = "300"
  records = ["accnginx.${var.zone_name}"]
}
