provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "${var.aws_region}"
}

#### Backend for tfstate file
##
terraform {
  backend "s3" {
    bucket         = "dyna-terraform-state"
    key            = "network.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "terraform-lock"
  }
}

data "template_file" "win_user_data" {
  template = "${file("../../templates/userdata.ps1")}"
}

module "vpc" {
  source       = "../../modules/vpc"
  project      = "${var.project}"
  aws_region   = "${var.aws_region}"
  aws_key_name = "${var.aws_key_name}"
  zone_id_priv = "${var.zone_id_priv}"
}

module "ansible" {
  source                 = "../../modules/ansible"
  aws_region             = "${var.aws_region}"
  security_group_default = "${module.vpc.security_group_default}"
  subnet_id              = "${module.vpc.subnet_private_id}"
  project                = "${var.project}"
  key_name               = "${var.aws_key_name}"
}

module "nginx" {
  source                 = "../../modules/nginx"
  aws_region             = "${var.aws_region}"
  key_name               = "${var.aws_key_name}"
  security_group_wfh     = "${module.vpc.security_group_wfh}"
  bitbucket_ipsv4_sg     = "${module.vpc.bitbucket_ipsv4_sg}"
  bitbucket_ipsv6_sg     = "${module.vpc.bitbucket_ipsv6_sg}"
  security_group_default = "${module.vpc.security_group_default}"
  subnet_id              = "${module.vpc.subnet_public_id}"
  project                = "${var.project}"
  zone_name              = "${var.zone_name}"
  zone_id                = "${var.zone_id}"
  zone_id_priv           = "${var.zone_id_priv}"
}

module "windows" {
  source                 = "../../modules/windows"
  aws_region             = "${var.aws_region}"
  key_name               = "${var.aws_key_name}"
  security_group_wfh     = "${module.vpc.security_group_wfh}"
  security_group_default = "${module.vpc.security_group_default}"
  subnet_id              = "${module.vpc.subnet_public_id}"
  project                = "${var.project}"
  zone_name              = "${var.zone_name}"
  zone_id                = "${var.zone_id}"
  instance_type          = "t2.micro"
  eip                    = "yes"
  ebs_volume_size        = "30"
  ami_win                = "ami-5041293f"
  root_vol_size          = "40"
  win_name               = "windowsterminal1"
  environment            = "infra"
  ssm                    = "no"
  ec2-ssm-role-profile   = ""
  win_user_data          = ""
  domain_name            = ""
}

module "windows2" {
  source                 = "../../modules/windows"
  aws_region             = "${var.aws_region}"
  key_name               = "${var.aws_key_name}"
  security_group_wfh     = "${module.vpc.security_group_wfh}"
  security_group_default = "${module.vpc.security_group_default}"
  subnet_id              = "${module.vpc.subnet_public_id}"
  project                = "${var.project}"
  zone_name              = "${var.zone_name}"
  zone_id                = "${var.zone_id}"
  eip                    = "no"
  instance_type          = "t3.micro"
  ebs_volume_size        = "30"
  ami_win                = "ami-5041293f"
  root_vol_size          = "40"
  win_name               = "windowsterminal2"
  environment            = "infra"
  ssm                    = "yes"
  ec2-ssm-role-profile   = "ec2-ssm-role-profile"
  win_user_data          = "${data.template_file.win_user_data.rendered}"
  domain_name            = "${aws_ssm_document.ssm_document_domain.name}"
}
