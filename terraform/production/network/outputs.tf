output "security_group_nat" {
  value = "${module.vpc.security_group_nat}"
}

output "subnet_private_id" {
  value = "${module.vpc.subnet_private_id}"
}

output "subnet_public_id" {
  value = "${module.vpc.subnet_public_id}"
}

output "security_group_wfh" {
  value = "${module.vpc.security_group_wfh}"
}

output "security_group_megafone" {
  value = "${module.vpc.security_group_megafone}"
}

output "security_group_default" {
  value = "${module.vpc.security_group_default}"
}

output "ssm_document_domain_name" {
  description = "ssm documnet for domain join"
  value       = "${aws_ssm_document.ssm_document_domain.name}"
}
