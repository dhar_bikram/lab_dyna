variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "aws_key_path" {}

variable "aws_region" {
  description = "EC2 Region for the VPC"
  default     = "eu-central-1"
}

variable "project" {
  description = "The resource project"
  default     = "techmlab"
}

variable "environment" {
  description = "Environment for the resource"
  default     = "OmniusBM"
}

variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.99.0.0/16"
}

variable "zone_id" {
  description = "ominusbm.com ZONE ID"
  default     = "XVFGRTRURS"
}
