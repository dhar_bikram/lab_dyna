var AWS = require("aws-sdk");

const _ = require("lodash");
const ec2 = new AWS.EC2({ apiVersion: "latest" });

function tagSnapshot(snapshotId) {
  var params = {
    Resources: [snapshotId],
    Tags: [
      {
        Key: "KubernetesCluster",
        Value: "kube.dynacommercelab.com"
      }
    ],
    DryRun: false
  };
  ec2.createTags(params, function (err, data) {
    if (err) {
      console.log(err, err.stack);
    }
    else {
      console.log("Successfully added tag to " + snapshotId);
    }
  });
}

function makeSnapshot(volumeId) {
  var snapshot_params = {
    VolumeId: volumeId,
    Description: "Backup of " + volumeId,
    DryRun: false
  };

  ec2.createSnapshot(snapshot_params, function (err, data) {
    if (err) {
      console.log(err, err.stack);
    }
    else {
      console.log("Successfully created snapshot of " + volumeId);
      tagSnapshot(data.SnapshotId);
    }
  });
}

var params = {
  DryRun: false,
  Filters: [
    {
      Name: "attachment.status",
      Values: ["attached"]
    },
    {
      Name: "tag:kubernetes.io/created-for/pvc/name",
      Values: [
        "sonar-bundled-plugins-sonar-0",
        "sonar-postgres-disk-sonar-postgres-0",
        "sonar-conf-sonar-0",
        "sonar-extensions-sonar-0",
        "sonar-data-sonar-0"
      ]
    }
  ]
};

ec2.describeVolumes(params, function (err, data) {
  if (err) {
    console.log(err, err.stack);
  }
  else {
    var volumes = _.map(data.Volumes, "VolumeId");
    _.map(volumes, makeSnapshot);
  }
});
