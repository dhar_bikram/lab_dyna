var url = require("url");
var AWS = require("aws-sdk");
var _ = require("lodash");
exports.handler = function(event, context) {
  AWS.config.region = "eu-central-1";

  var ec2 = new AWS.EC2({ apiVersion: "latest" });

  var deleteSnapshot = function(snapshot_id) {
    var snapshot_params = {
      SnapshotId: snapshot_id,
      DryRun: false
    };

    ec2.deleteSnapshot(snapshot_params, function(err, data) {
      if (err) console.log(err, err.stack);
      else {
        console.log("Successfully deleted " + snapshot_id);
      }
    });
  };

  var params = {
    Filters: [
      {
        Name: "status",
        Values: ["completed"]
      },
      {
        Name: "tag:KubernetesCluster",
        Values: ["kube.dynacommercelab.com"]
      }
    ]
  };

  var getExpired = function(snapshot) {
    return (
      new Date(
        new Date(snapshot.StartTime).getTime() + 30 * 24 * 60 * 60 * 1000
      ) < new Date()
    );
  };

  ec2.describeSnapshots(params, function(err, data) {
    if (err) console.log(err, err.stack);
    else var expired_snapshots = _.filter(data.Snapshots, getExpired);
    var expired_snapshot_ids = _.map(expired_snapshots, "SnapshotId");
    _.map(expired_snapshot_ids, deleteSnapshot);
  });
};

